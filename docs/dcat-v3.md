## 소개
서로 다른 조직, 연구자, 정부 및 시민 간에 데이터 리소스를 공유하려면 데이터 공개 여부와 관계가 없이 메타데이터를 제공해야 한다. DCAT는 웹에 데이터 카탈로그를 게시하기 위한 용어로, 원래는 [data.gov](https://www.data.gov/)와 [data.gov.uk](https://data.gov.uk/)과 같은 정부 데이터 카탈로그의 컨텍스트에서 개발되었지만, 다른 컨텍스트에서도 적용 가능하여 이를 사용하게 되었다.

DCAT 3은 이전 버전을 확장하여 [추가 사용 사례와 요구 사항](https://www.w3.org/TR/dcat-ucr/)을 지원한다. 데이터세트 시리즈와 같은 데이터세트 외에도 다른 리소스를 카탈로깅할 수 있는 가능성을 포함하고 있다. 이 V3.0은 또한 리소스의 버전에 대한 설명도 작성할 수 있으며 역속성(inverse properties) 사용 방법에 대한 지침을 제공한다.

DCAT는 데이터세트와 데이터 서비스를 기술하고 카탈로그에 포함할 수 있도록 RDF 클래스와 속성을 제공한다. 표준 모델과 용어를 사용하면 여러 카탈로그에서 메타데이터를 쉽게 활용하고 종합할 수 있다.

1. 데이터세트와 데이터 서비스의 탐색성 향상
2. 다중 사이트 카탈로그로부터 데이터세트 연합 검색 허용

카탈로그에 기술된 데이터는 스프레드시트부터 XML, RDF 등 다양한 특수 형식에 이르기까지 다양한 포맷으로 제공될 수 있다. DCAT은 데이터세트의 이러한 serialization 형식에 대해 어떠한 가정도 하지 않지만 추상 데이터세트와 다른 명세 또는 배포를 구별할 뿐이다.

종종 기존 데이터로부터 추출, 부분 집합 또는 조합을 지원하는 서비스 또는 일부 데이터 처리 기능에 의해 생성된 새로운 데이터의 선택을 지원하는 서비스를 통해 데이터를 제공한다. DCAT은 데이터 접근 서비스에 대한 기술을 카탈로그에 포함할 수 있다.

보완적 어휘를 DCAT와 함께 사용하여 보다 상세한 형식별 정보를 제공할 수 있다. 예를 들어, 데이터세트가 RDF 형식인 경우 데이터세트에 대한 다양한 통계를 표현하기 위해 DCAT 내에서 [VoID](https://www.w3.org/TR/void/) 어휘의 속성을 사용할 수 있다.

이 문서에서는 DCAT으로 표현된 데이터 카탈로그에 적용하는 특정 방법을 규정하지 않는다. SPARQL endpoint를 통해 접근할 수 있는 RDF, HTML 페이지에 내장된 HTML-RDFa([HTML+RDFa 1.1 - Second Edition](https://www.w3.org/TR/html-rdfa/)), RDF/XML([RDF 1.1 XML Syntax](https://www.w3.org/TR/rdf-syntax-grammar/)), N3([Notation3 (N3): A readable RDF syntax](https://www.w3.org/TeamSubmission/2008/SUBM-n3-20080114/)), Turtle([[RDF 1.1 Turtle]](https://www.w3.org/TR/turtle/)), JSON-LD([JSON-LD 1.0](https://www.w3.org/TR/json-ld/)) 또는 기타 형식으로 DCAT 정보를 serialize할 수 있다. 이 문서에서 예는 가독성 때문에 Turtle([[RDF 1.1 Turtle]](https://www.w3.org/TR/turtle/))을 사용한다.

> **NOTE**

> 자세한 [어휘 사양](#vocabulary-spec)은 이 문서에서 편역하지 않았으며 향후 다른 페이지에서 다루도록 할 것이다.

## Motivation of change
2014년 1월에 발표된 원래의 권고안 [VOCAB-DCAT-1](https://www.w3.org/TR/vocab-dcat-1/)은 데이터세트를 설명하기 위한 기본 프레임워크를 제공했다. 이는 추상적인 아이디어로서의 *데이터세트*와 데이터세트의 명세으로서의 *배포* 사이에 중요한 차이를 만들었다. 비록 DCAT이 널리 채택되었지만, European Commission의 [DCAT-AP](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)와 같은 프로파일의 메커니즘이나 [의료 및 생명과학 커뮤니티 프로파일](https://www.w3.org/TR/hcls-dataset/), [Data Tag Suite](https://datatagsuite.github.io/docs/html/) 등과 같이 기본 표준에 기반하는 크거나 작은 범위로 구축된 더 많은 어휘의 개발을 통해 추가된 많은 필수적인 특징들이 원래 규격에는 부족하다는 것이 명백해졌다. 이러한 더 큰 어휘의 출력 간 상호운용성을 목적으로 다양한 커뮤니티의 경험을 통해 밝혀진 특정 단점을 해결하기 위해 [DCAT 2](https://www.w3.org/TR/vocab-dcat-2/)를 개발하였다. 예를 들어, DCAT 2는 [식별자](#dereferenceable-identifiers), [데이터세트 품질 정보](#quality-information)와 [데이터 인용 문제](#data-citation) 이슈를 해결하기 위한 클래스, 속성과 지침을 제공했다.

이 개정판인 DCAT 3은 사양을 전체적으로 업데이트한다. 2014년 권고안과 DCAT 2의 중요한 변경 사항을 본문 내에 "Note" 표시로 설명한다.

## Namespaces
DCAT의 namespace는 `http://www.w3.org/ns/dcat#`이다.  또한 DCAT는 다른 어휘(vocabulary)의 용어, 특히 [Dublin Core](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)의 용어를 광범위하게 사용한다. 그러므로 DCAT는 자체 클래스와 속성의 최소 집합만을 정의한다.

### 공식 namespaces
권고안에서 공식적으로 사용되는 namespace와 접두사(prefix)는 아래 표와 같다.

| Prefix | Namespace IRI | Source |
|--------|---------------|--------|
| dc | http://purl.org/dc/elements/1.1/ | [DCTERMS](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) |
| dcat | http://www.w3.org/ns/dcat# | [VOCAB-DCAT](https://www.w3.org/TR/vocab-dcat/) |
| dcterm | http://purl.org/dc/terms/ | [DCTERMS](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) |
| dctype | http://purl.org/dc/dcmitype/ | [DCTERMS](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) |
| foaf | http://xmlns.com/foaf/0.1/ | [FOAF](http://xmlns.com/foaf/spec) |
| locn | http://www.w3.org/ns/locn# | [LOCN](http://www.w3.org/ns/locn) |
| odrl | http://www.w3.org/ns/odrl/2/ | [ODRL-VOCAB](https://www.w3.org/TR/odrl-vocab/) |
| owl | http://www.w3.org/2002/07/owl# | [OWL2-SYNTAX](https://www.w3.org/TR/owl2-syntax/) |
| prov | http://www.w3.org/ns/prov# | [PROV-O](https://www.w3.org/TR/prov-o/) |
| rdf | http://www.w3.org/1999/02/22-rdf-syntax-ns# | [RDF-SYNTAX-GRAMMAR](https://www.w3.org/TR/rdf-syntax-grammar/) |
| rdfs | http://www.w3.org/2000/01/rdf-schema# | [RDF-SCHEMA](https://www.w3.org/TR/rdf-schema/) |
| skos | http://www.w3.org/2004/02/skos/core# | [SKOS-REFERENCE](https://www.w3.org/TR/skos-reference/) |
| spdx | http://spdx.org/rdf/terms# | [SPDX](http://spdx.org/rdf/terms#) |
| time | http://www.w3.org/2006/time# | [OWL-TIME](https://www.w3.org/TR/owl-time/) |
| vcard | http://www.w3.org/2006/vcard/ns# | [VCARD-RDF](https://www.w3.org/TR/vcard-rdf/) |
| xsd| http://www.w3.org/2001/XMLSchema# | [XMLSCHEMA11-2](https://www.w3.org/TR/xmlschema11-2/) |

### 비공식 namespaces
권고안에서 표준이 아닌 문서의 예시와 지침에 사용된 namespace와 접두사는 다음과 같다.

| Prefix | Namespace IRI | Source |
|--------|---------------|--------|
| adms| https://www.w3.org/ns/adms# | [VOCAB-ADMS](https://www.w3.org/TR/vocab-adms/) |
| dqv | http://www.w3.org/ns/dqv# | [VOCAB-DQV](https://www.w3.org/TR/vocab-dqv/) |
| earl | http://www.w3.org/ns/earl# | [EARL10-Schema](https://www.w3.org/TR/EARL10-Schema/) |
| geosparql | http://www.opengis.net/ont/geosparql# | [GeoSPARQL](http://www.opengeospatial.org/standards/geosparql) |
| oa | http://www.w3.org/ns/oa# | [ANNOTATION-VOCAB](https://www.w3.org/TR/annotation-vocab/) |
| pav | http://purl.org/pav/ | [PAV](https://pav-ontology.github.io/pav/) |
| sdmx-attribute | http://purl.org/linked-data/sdmx/2009/attribute# | [VOCAB-DATA-CUBE](https://www.w3.org/TR/vocab-data-cube/) |
| sdo | https://schema.org/ | [SCHEMA-ORG]( https://schema.org/) |
| xhv | http://www.w3.org/1999/xhtml/vocab# | [XHTML-VOCAB](https://www.w3.org/1999/xhtml/vocab) |

## 적합성(Conformance) <a id="conformance"></a>
비규범으로 표시된 절뿐만 아니라 본 명세서의 모든 저작 지침, 다이어그램, 예제 및 노트는 규범적이지 않다. 이 명세서의 다른 모든 것들은 규범적이다.

이 문서의 키워드 *MAY*, *MUST*, *MUST NOT*과 *SHOULD*는 여기 표시된 것처럼 모두 대문자로 표시될 때와 나타날 때만 [BCP 14](https://datatracker.ietf.org/doc/html/bcp14)([RFC 2119](https://www.rfc-editor.org/rfc/rfc2119), [RFC 8174](https://www.rfc-editor.org/rfc/rfc8174))에 기술된 대로 해석해야 한다. (영어 문서에만 해당)

다음과 같은 경우에 데이터 카탈로그는 DCAT을 준수한다.

- 데이터에 대한 액세스는 데이터세트, 배포와 데이터 서비스로 이루어진다.
- 카탈로그 자체, 해당 카탈로그를 구성하는 카탈로그된 리소스와 배포에 대한 RDF 서술을 사용할 수 있지만 RDF 구문, 액세스 프로토콜과 액세스 정책을 이 사양에서 규정하지 않는다.
- 카탈로그에 보관되어 있고 카탈로그 자체, 해당 카탈로그를 구성하는 카탈로그된 리소스 및 배포에 대한 데이터를 포함하는 모든 메타데이터 필드의 내용은 RDF 기술을 포함하고 있으며, 이러한 클래스 또는 속성이 없는 경우를 제외하고 DCAT의 적절한 클래스와 속성을 사용하여 표현한다.
- DCAT에 정의된 모든 클래스와 속성을 이 명세에서 선언된 의미와 일치하는 방식으로 사용한다.

DCAT 호환 카탈로그에는 카탈로그를 RDF로 기술할 때 DCAT에서 정의하지 않은 메타데이터 필드와 RDF 데이터를 *추가할 수* 있다.

**DCAT 프로파일**은 DCAT에 추가적인 제약 조건을 추가한 데이터 카탈로그에 대한 사양이다. 프로파일을 준수하는 데이터 카탈로그는 DCAT도 준수한다. 프로파일의 추가 제약 조건은 다음과 같다.

- 필수 메타데이터 필드의 최소 집합을 포함한 카디널리티 제약 조건
- 표준 DCAT 클래스와 속성의 하위 클래스와 하위 속성
- DCAT 용어 사양에서 명시되지 않은 추가적인 메타데이터 필드에 대한 클래스와 속성
- 속성에 허용되는 값을 정의한 어휘 또는 IRI 세트
- 카탈로그의 RDF 기술의 특정 접근 메커니즘(RDF 구문, 프로토콜)에 대한 요구 사항

> **NOTE** <br>
> 이 문서에서 사용되는 프로파일의 개념은 Dublin Core 커뮤니티에서 *[어플리케이션 프로파일](http://dublincore.org/documents/profile-guidelines/)*이라고 부르는 메타데이터 사양을 나타낸다.

## 어휘 개요(Vocabulary overview)

### DCAT 범위
DCAT은 데이터 카탈로그를 나타내기 위한 RDF 어휘이다. DCAT는 아래 주요 일곱 클래스를 기반으로 구성되어 있다 ([Fig. 1](#fig-1)).

- `dcat:Catalog`는 카탈로그를 나타낸다. 이는 각 개별 항목이 일부 리소스를 기술하는 메타데이터 레코드인 데이터세트이다. `dcat:Catalog`의 범위는 **데이터세트**, **데이터 서비스** 또는 **기타 리소스 타입들**에 대한 메타데이터의 컬렉션(collection)이다.
- `dcat:Resource`는 카탈로그의 메타데이터 레코드가 기술하고 있는 데이터 세트, 데이터 서비스 또는 기타 리소스들을 나타낸다. 이 클래스는 직접 사용되지는 않지만 `dcat:Dataset`, `dact:DataService`와 `dcat:Catalog`의 상위 클래스이다. 카탈로그에 있는 리소스는 이 클래스들, 이 클래스의 하위 클래스들, DCAT 프로파일 또는 기타 DCAT 어플리케이션에 정의된 리소스의 `dcat:Resource`의 하위 클래스들의 중 한 인스턴스여야 한다. `dcat:Resource`는 실제로 모든 종류의 리소스 카탈로그를 정의하는 연장점(extension point)이다. 일부 데이터세트와 서비스인 `dcat:Dataset`과 `dact:DataService`는 카탈로그에 나타나지 않을 수 있다.
- `dcat:Dataset`은 단일 에이전트 또는 식별 가능한 커뮤니티가 게시하거나 큐레이션된 데이터 컬렉션을 나타낸다. 모든 커뮤니티에서 발생하는 리소스 타입을 수용할 수 있도록 DCAT의 데이터세트 개념은 광범위하고 포괄적이다. 숫자, 텍스트, 픽셀, 이미지, 소리, 기타 멀티미디어 등 잠재적으로 데이터세트가 갖을 수 있는 모든 데이터 타입을 여러 형태의 데이터로 제공한다.
- `dcat:Distribution`은 다운로드 가능한 파일과 같이 데이터세트의 접근 가능한 방법을 나타낸다.
- `dcat:DataService`는 하나 이상의 데이터세트 또는 데이터 처리 기능에 대한 액세스를 제공하는 인터페이스(API)를 통해 데이터 액세스할 수 있는 작업(함수)들의 컬렉션을 나타낸다.
- `dcat:DatasetSeries`는 별도로 게시된 데이터세트의 컬렉션을 나타내지만 멤버 데이터세트들의 특성을 공유하는 데이터세트 그룹이다.
- `dcat:CatalogRecord`는 카탈로그의 메타데이터 레코드를 나타내며, 주로 레코드를 추가한 사용자와 시간같이 등록 정보와 관련이 있는 것들이다.

![Fig. 1](images/dcat-all-attributes.svg)
<center>Fig. 1 DCAT 모델 개요</center> <a id="fig-1"></a>

> **NOTE** <br>
> 이절의 다음부터와 이 다이어그램은 **규범이 아니다**. 또한 이 다이어그램은 UML 스타일 클래스 표기법을 사용하지만 속성, 관계 및 카디널리티의 유무에 대하여는 일반적인 RDF 오픈 월드 가정을 따라 해석해야 한다. 각 클래스에 표시된 속성은 다음 절인 [용어 사양]()의 클래스에 대한 기술에 명시된 속성을 반영한다. 열린 화살표는 RDFS *subclass-of* 관계(객체 지향 일반화가 아님)를 나타낸다. 각 클래스의 전체 범위를 이해하는 데 도움이 되도록 사용 가능한 속성을 각 '::super-class'에서 복사한다.

DCAT에서 **데이터세트(dataset)**를 "단일 에이전트에 의해 게시 또는 큐레이션되는 하나 이상의 직렬화 또는 포맷으로 액세스하거나 다운로드할 수 있는 데이터의 컬레션"으로 정의한다. 데이터세트는 개념적인 엔티티이며 전송을 위해 데이터세트를 직렬화된 하나 이상의 **배포(distribution)**로 표현될 수 있다. 데이터세트의 배포는 **데이터 서비스(datat service)**를 통해 이루어진다.

데이터 서비스는 로컬 또는 원격으로 호스팅할 수 있는 데이터세트에 일반적으로 선택, 추출, 결합, 처리 또는 변환 작업을 서비스를 제공한다. 데이터 세트 또는 카탈로그의 일부 또는 전부를 얻기 위하여 데이터 서비스를 요청한다. 데이터 서비스는 특정 데이터세트에 연결되거나 요청 시 또는 런타임에 소스 데이터를 구성할 수 있다. 데이터 배포 서비스를 수행할 때 데이터세트 또는 서브세트의 배포를 선택하고 다운로드할 수 있다. 데이터 탐색 서비스를 통해 클라이언트는 적절한 데이터세트를 찾을 수 있다. 다른 종류의 데이터 서비스로 좌표 변환 서비스, 재샘플링 및 보간 서비스, 시뮬레이션 및 모델링 서비스를 포함하는 다양한 데이터 변환 서비스를 포함한다. DCAT의 데이터 서비스는 데이터에 대한 액세스를 제공하는 작업의 모음 또는 **API**로 제공된다. 대화형 사용자 인터페이스를 종종 API 작업에 대한 편리한 접근을 제공하기 위해 사용할 수 있지만, 그에 대한 설명은 이 문서의 범위가 아니어 다루지 않는다. 특정 데이터 서비스 엔드포인트의 세부 사항을 종종 표준 서비스 타입을 따르는 기술로 명시한다. 이로서 DCAT 어휘 범위를 보완하고 있다.

**카탈로그**에 데이터세트와 데이터 서비스에 대한 기술을 포함할 수 있다. 카탈로그는 데이터세트의 일종으로, 데이터세트 및 데이터 서비스 들을 설명하는 항목으로 구성되어 있다. 다른 타입의 리소스도 카탈로깅할 수 있지만, 현재는 DCAT의 범위를 데이터세트와 데이터 서비스로 한정하고 있다. 카탈로그의 범위를 데이터세트 및 데이터 서비스 이상으로 확장하려면 DCAT 프로필 또는 기타 DCAT 어플리케이션에서 `dcat:Resource`의 추가 하위 클래스를 정의하는 것을 권고한다. 데이터 배포 서비스를 너머 서비스 기술 범위를 확장하려면 DCAT 프로필 또는 기타 DCAT 어플리케이션(DCAT-AP)에서 `dcat:DataService`의 추가 하위 클래스를 정의하는 것을 추천한다.

> **NOTE**
> [DCAT 1](https://www.w3.org/TR/vocab-dcat-1/)의 범위는 데이터세트 카탈로그로 제한되었다. [개정판](https://www.w3.org/TR/dcat-ucr/)의 수많은 사용 사례는 카탈로그의 구성원으로서 데이터 서비스를 포함한다. [웹 서비스 기술을 위한 DCAT 배포](https://www.w3.org/TR/dcat-ucr/#ID6)와 [서비스 기반 데이터 액세스 모델링](https://www.w3.org/TR/dcat-ucr/#ID18)을 참조하시오. 따라서 DCAT 2의 범위는 DCAT을 준수하는 카탈로그의 일부가 될 수 있도록 데이터세트와 데이터 서비스 모두를 포함한다. 다른 카탈로그들로 구성된 카탈로그에 대하여도 규정하고 있다. 

> 다른 종류의 카탈로그를 DCAT 패턴에 따라 설계할 수 있다 (예: 시설, 도구, 샘플 및 표본, 기타 물리적 인공물, 이벤트 또는 활동 등). 이것들은 현재 DCAT의 범위 밖에 있지만 DCAT 프로필이나 다른 DCAT 어플리케이션에서 명시할 수 있는 `dcat:Resource`의 추가 하위 클래스를 통해 정의될 수 있다.

**카탈로그 레코드**는 카탈로그의 레코드을 설명한다. `dcat:Resource`는 데이터세트 또는 서비스 자체를 나타내는 반면 `dcat:CatalogRecord`는 카탈로그에 있는 리소스의 등록을 기술하는 레코드이다. `dcat:CatalogRecord` 사용은 선택 사항이다. 카탈로그의 항목에 대한 출처 정보를 명시적으로 기록하는 데 사용한다. 필요하지 않으면 `dcat:CatalogRecord`를 사용하지 않을 수 있다.

### RDF 고려사항(considerations)
DCAT 어휘는 [RDF 스키마](https://www.w3.org/TR/rdf-schema/)를 사용하여 [OWL2 온톨로지](https://www.w3.org/TR/owl2-overview/)를 정형화한 것이다. [IRI](https://www.rfc-editor.org/rfc/rfc3987)(RFC3987)로 DCAT의 각 클래스와 속성을 표기한다. 로컬에서 정의한 요소는 네임스페이스 `http://www.w3.org/ns/dcat#`에 있다. 또한 여러 외부 어휘, 특히 [FOAF Vocabulary Specification 0.99 (Paddington Edition)](http://xmlns.com/foaf/spec), [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)과 [PROV-O]( https://www.w3.org/TR/prov-o/)에서 요소를 채택하고 있다.

RDF를 사용하면 리소스는 IRI(글로벌 식별자)를 가지고 있거나 빈 노드일 수 있다. IRI를 사용하여 명시적으로 이름을 지정하지 않고 리소스를 나타내기 위하여 빈 노드를 사용할 수 있다. 빈 노드는 [triple](https://sdldocs.gitlab.io//rdf/rdf-primer/)의 subject와 object 위치에 나타날 수 있다. 예를 들어, 많은 실제 DCAT 카탈로그에서 관련 데이터세트 기술 내에서 빈 노드로 배포를 표현하고 있다. 빈 노드는 일부 사용 사례에 유연성을 제공할 수 있지만 Linked Data 컨텍스트에서 빈 노드는 데이터에 공동으로 주석을 달 수 있는 능력을 제한한다. 빈 노드 리소스는 링크의 대상이 될 수 없으며 새로운 소스에 새로운 정보로 주석을 달 수 없다. Linked Data 접근 방식의 가장 큰 이점 중 하나는 "누구나 어디서든 어떤 것을 표현할 수 있다"는 것이므로 빈 노드를 사용하면 RDF 모델의 광범위한 사용으로 얻을 수 있는 이점 중 일부가 약화된다. 단일 애플리케이션 데이터 세트의 닫힌 세계(closed world) 내에서도 새 데이터를 통합할 때 빈 노드의 사용이 빠르게 제한될 수 있다([Linked Data Patterns: A pattern catalogue for modelling, publishing, and consuming Linked Data](http://patterns.dataincubator.org/book/)). 이러한 이유에서 DCAT 기본 클래스의 인스턴스에 전역 식별자를 권장되며, 일반적으로 RDF에서 DCAT를 인코딩할 때 빈 노드를 사용을 권장하지 않는다.

이 문서의 모든 RDF 예는 [Turtle 구문](https://www.w3.org/TR/turtle/)으로 작성되었으며 대부분 [DXWG 코드 저장소](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)에서 볼 수 있다.

> **NOTE**
> 이 문서의 RDF 예들은 DCAT의 특정 기능을 보여주기 위한 것이므로 완전한 DCAT 리소스에 나타날 수 있는 잠재적 속성과 링크의 부분 집합만을 보여준다.

### 기본 예
이 예에서는 DCAT를 사용하여 정부 카탈로그와 해당 데이터세트를 나타내는 방법에 대한 간략한 개요를 보인다. 언어 태그 사용을 보여주기 위해 제목, 레이블 및 키워드를 영어와 스페인어로 작성하였다.

첫째, 카탈로그 설명:

> EXAMPLE 1 <a id="ex-1"></a>
```
ex:catalog
  a dcat:Catalog ;
  dcterms:title "Imaginary Catalog"@en ;
  dcterms:title "Catálogo imaginario"@es ;
  rdfs:label "Imaginary Catalog"@en ;
  rdfs:label "Catálogo imaginario"@es ;
  foaf:homepage <http://dcat.example.org/catalog> ;
  dcterms:publisher ex:transparency-office ;
  dcterms:language <http://id.loc.gov/vocabulary/iso639-1/en>  ;
  dcat:dataset ex:dataset-001 , ex:dataset-002 , ex:dataset-003 ;
  .
```

카탈로그 게시자(publisher)로 상대적 IRI `ex:transparency-office`를 가지고 있다. [EXAMPLE 2](#ex-2)와 같이 게시자에 대한 추가 기술를 제공할 수 있다.

> EXAMPLE 2 <a id="ex-2"></a>
```
ex:transparency-office
  a foaf:Organization ;
  rdfs:label "Transparency Office"@en ;
  rdfs:label "Oficina de Transparencia"@es ;
  .
```

카탈로그는 `dcat:dataset` 속성을 통해 각 데이터 세트를 나열한다. [EXAMPLE 1](#ex-1)에서는 상대 IRI `ex:dataset-001`를 사용하여 데이터세트 예를 언급하였다. DCAT을 사용한 기술은 다음과 같다.

> EXAMPLE 3 <a id="ex-3"></a>

```
ex:dataset-001
  a dcat:Dataset ;
  dcterms:title "Imaginary dataset"@en ;
  dcterms:title "Conjunto de datos imaginario"@es ;
  dcat:keyword "accountability"@en, "transparency"@en, "payments"@en ;
  dcat:keyword "responsabilidad"@es, "transparencia"@es, "pagos"@es ;
  dcterms:creator ex:finance-employee-001 ;
  dcterms:issued "2011-12-05"^^xsd:date ;
  dcterms:modified "2011-12-15"^^xsd:date ;
  dcat:contactPoint <http://dcat.example.org/transparency-office/contact> ;
  dcterms:temporal [ a dcterms:PeriodOfTime ;
    dcat:startDate "2011-07-01"^^xsd:date ; 
    dcat:endDate   "2011-09-30"^^xsd:date ;
  ];
  dcat:temporalResolution "P1D"^^xsd:duration ;
  dcterms:spatial <http://sws.geonames.org/6695072/> ;
  dcat:spatialResolutionInMeters "30.0"^^xsd:decimal ;
  dcterms:publisher ex:finance-ministry ;
  dcterms:language <http://id.loc.gov/vocabulary/iso639-1/en> ;
  dcterms:accrualPeriodicity <http://purl.org/linked-data/sdmx/2009/code#freq-W>  ;
  dcat:distribution ex:dataset-001-csv ;
  .
```

위의 데이터세트에는 고유한 시간 기술자(descriptor)가 다섯개 있디. `dcterms:issued`와 `dcterms:modified`는 각각 데이터세트 발행과 변경 날짜이다. `dcterms:accrualPeriodicity`는 데이터세트 업데이트 빈도로 [W3C Data Cube Vocabulary](https://www.w3.org/TR/vocab-data-cube/)에 대한 노력의 일환으로 개발된 [콘텐츠 지향 지침](https://www.w3.org/TR/vocab-data-cube/#dsd-cog)의 인스턴스를 사용한다. `dcterms:temporal`에서 `dcat:startDate`과 `dcat:endDate`으로 표시되는 `dcterms:PeriodOfTime`은 닫힌 간격으로 시간 범위 또는 확장을 정의한다. 표준 데이터 유형 `xsd:duration`을 사용하여 `dcat:temporalResolution`은 데이터세트 항목간의 최소 간격을 설명하는 시간 해상도를 제공한다.

또한 공간 범위 또는 확장을 [Geonames](http://www.geonames.org/)의 IRI를 사용하여 `dcterms:spatial`로 지정한다. 데이터세트 내 항목의 최소 공간 분리를 기술하는 공간 해상도는 표준 데이터 타입 `xsd:decimal`을 사용하여 `dcat:spatialResolutionInMeters`에 주어진다.

데이터세트에 대한 의견과 피드백을 보낼 수 있는 연락처를 제공한다. 이메일 주소나 전화번호와 같은 연락처에 대한 자세한 내용은 [vCard](https://www.w3.org/TR/vcard-rdf/)를 사용하여 제공할 수 있다.

데이터세트 `ex:dataset-001-csv`를 5kB CSV 파일로 다운로드할 수 있다. 이것은 `dcat:Distribution` 타입의 RDF 리소스로 표시된다.

> EXAMPLE 4
```
ex:dataset-001-csv
  a dcat:Distribution ;
  dcat:downloadURL <http://dcat.example.org/files/001.csv> ;
  dcterms:title "CSV distribution of imaginary dataset 001"@en ;
  dcterms:title "distribución en CSV del conjunto de datos imaginario 001"@es ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/csv> ;
  dcat:byteSize "5120"^^xsd:nonNegativeInteger ;
  .
```

### 테마별 데이터세트 분류
카탈로그는 상대 IRI `ex:themes`로 표시된 도메인 세트에 따라 데이터세트를 분류합니다. [SKOS](https://www.w3.org/TR/skos-reference/)는 사용하는 도메인을 기술하는 데 사용할 수 있다.

> EXAMPLE 5
```
ex:catalog dcat:themeTaxonomy ex:themes .

ex:themes
  a skos:ConceptScheme ;
  skos:prefLabel "A set of domains to classify documents"@en ;
  .

ex:dataset-001 dcat:theme ex:accountability .
```

이 데이터세트는 상대적 IRI `ex:accountability`로 표시된 도메인으로 분류된다. 카탈로그 도메인을 기술하는 데 사용된 IRI `ex:themes`가 식별하는 개념 스티마의 일부로 개념을 정의하는 것을 권장한다. SKOS 기술의 예는 아래와 같다.

> EXAMPLE 6
```
ex:accountability
  a skos:Concept ;
  skos:inScheme ex:themes ;
  skos:prefLabel "Accountability"@en ;
  .
```

### 데이터세트 타입 분류
데이터 세트의 타입 또는 장르는 `dcterms:type` 속성을 사용하여 표시할 수 있다. 속성 값은 [DCMI Type Vocabulary](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#section-7)([DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)), [MARC Genre/Terms Scheme](https://id.loc.gov/vocabulary/marcgt.html), [Geographic information -- Metadata](https://www.iso.org/standard/26020.html) [MD_Scope Code](https://standards.iso.org/iso/19115/resources/Codelists/gml/MD_ScopeCode.xml), [DataCite resource type](https://schema.datacite.org/meta/kernel-4.4/include/datacite-resourceType-v4.xsd) ([DataCite Metadata Schema](https://schema.datacite.org/)) 또는 Re3data([Metadata Schema for the Description of Research Data Repositories: version 3](https://doi.org/10.2312/re3.008))의 PARSE.Insight content-type 등과 같이 잘 통제되고 널리 인정된 자원 타입 집합에서 가져오는 것을 권장한다.

다른 어휘의 값을 사용하여 다음 예의 데이터세트를 별도로 분류한다.

> EXAMPLE 7
```
ex:dataset-001
  rdf:type  dcat:Dataset ;
  dcterms:type  <http://purl.org/dc/dcmitype/Dataset> ;
  .

ex:dataset-001
  rdf:type  dcat:Dataset ;
  dcterms:type  <http://id.loc.gov/vocabulary/marcgt/dtb> ;
  .
```

하나의 기술에 여러 다른 분류가 존재할 수도 있다.

> EXAMPLE 8
```
ex:dataset-001
  rdf:type  dcat:Dataset ;
  dcterms:type  <http://purl.org/dc/dcmitype/Dataset> ;
  dcterms:type  <http://id.loc.gov/vocabulary/marcgt/dtb> ;
  dcterms:type  <http://registry.it.csiro.au/def/datacite/resourceType/Dataset> ;
  dcterms:type  <http://registry.it.csiro.au/def/re3data/contentType/database> ;
.

<http://registry.it.csiro.au/def/datacite/resourceType/Dataset>
  rdfs:label "Dataset"@en ;
  dcterms:source [ rdfs:label "DataCite resource types"@en ] ;
  .

<http://registry.it.csiro.au/def/re3data/contentType/database>
  rdfs:label "Database"@en ;
  dcterms:source [ rdfs:label "Re3data content types"@en ] ;
  .
```

### 카탈로그 레코드 메터데이터 기술
카탈로그 게시자가 레코드를 기술하는 메타데이터를 유지하기로 결정한 경우(즉, 데이터세트를 기술하는 메타데이터를 포함하는 레코드) `dcat:CatalogRecord`를 사용할 수 있다. 예를 들어, `ex:dataset-001`은 `2011-12-05`에 게시되었지만, Imaginary Catalog에 대한 기술을 2011-12-11에 추가하였다. 이를 [EXAMPLE 9](#ex-9)에서와 같이 DCAT으로 나타낼 수 있다.

> EXAMPLE 9 <a id="ex-9"></a>
```
ex:catalog dcat:record ex:record-001  .

ex:record-001
  a dcat:CatalogRecord ;
  foaf:primaryTopic ex:dataset-001 ;
  dcterms:issued "2011-12-11"^^xsd:date ;
  .
```

### 웹 페이지 뒤에서만(에서 안보이지만) 사용할 수 있는 데이터세트
ex:dataset-002는 CSV 파일로 가용하다. 그러나 `ex:dataset-002`는 사용자가 링크를 따라가서 웹 페이지를 통해서만 얻을 수 있지만, 정보를 제공하고 데이터를 액세스하기 전에 checkbox를 선택할 수 있다.

> EXAMPLE 10
```
ex:dataset-002
  a dcat:Dataset ;
  dcat:landingPage <http://dcat.example.org/dataset-002.html> ;
  dcat:distribution ex:dataset-002-csv ;
  .
ex:dataset-002-csv
  a dcat:Distribution ;
  dcat:accessURL <http://dcat.example.org/dataset-002.html> ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/csv> ;
  .
```

`dcat:landingPage`의 사용과 `dcat:Distribution` 인스턴스의 정의를 한다.

### 다운로드할 수 있으며 일부 웹 페이지 뒤에서 사용할 수 있는 데이터세트
반면 `ex:dataset-003`은 랜딩 페이지를 통해 얻을 수 있지만 알려진 URL에서 다운로드받을 수도 있다.

> EXAMPLE 11
```
ex:dataset-003
  a dcat:Dataset ;
  dcat:landingPage <http://dcat.example.org/dataset-003.html> ;
  dcat:distribution ex:dataset-003-csv ;
  .
ex:dataset-003-csv
  a dcat:Distribution ;
  dcat:downloadURL <http://dcat.example.org/dataset-003.csv> ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/csv> ;
  .
```

다운로드 가능한 배포와 함께 `dcat:downloadURL`을 사용했으며 랜딩 페이지를 통해 액세스할 수 있는 다른 배포를 별도의 `dcat:Distribution` 인스턴스로 정의할 필요는 없다.

### 서비스를 통해 사용 가능한 데이터세트
`ex:dataset-004`를 다른 서비스의 다른 표현으로 배포한다. 각 `dcat:Distribution`에 대한 `dcat:accessURL`은 서비스의 `dcat:endpointURL`에 해당한다. `dcterms:conformsTo`를 사용하는 특정 API 정의인 `dcterms:type`을 사용하는 일반 타입(여기에서는 INSPIRE 공간 데이터 서비스 타입 어휘의 값 사용)이 각 서비스를 `dcat:endpointDescription`을 사용하여 연결된 개별 엔드포인트 매개변수와 옵션에 대한 자세한 기술로 특성화한다. 

> EXAMPLE 12

```
ex:dataset-004
  rdf:type dcat:Dataset ;
  dcat:distribution ex:dataset-004-csv ;
  dcat:distribution ex:dataset-004-png ;
  .

ex:dataset-004-csv
  rdf:type dcat:Distribution ;
  dcat:accessService ex:table-service-005 ;
  dcat:accessURL <http://dcat.example.org/api/table-005> ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/csv> ;
  .

ex:dataset-004-png
  rdf:type dcat:Distribution ;
  dcat:accessService ex:figure-service-006 ;
  dcat:accessURL <http://dcat.example.org/api/figure-006> ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/image/png> ;
  .

ex:figure-service-006
  rdf:type dcat:DataService ;
  dcterms:conformsTo <http://dcat.example.org/apidef/figure/v1.0> ;
  dcterms:type <https://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/view> ;
  dcat:endpointDescription <http://dcat.example.org/api/figure-006/params> ;
  dcat:endpointURL <http://dcat.example.org/api/figure-006> ;
  dcat:servesDataset ex:dataset-004 ;
  .

ex:table-service-005
  rdf:type dcat:DataService ;
  dcterms:conformsTo <http://dcat.example.org/apidef/table/v2.2> ;
  dcterms:type <https://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/download> ;
  dcat:endpointDescription <http://dcat.example.org/api/table-005/capability> ;
  dcat:endpointURL <http://dcat.example.org/api/table-005> ;
  dcat:servesDataset ex:dataset-003, ex:dataset-004 ;
  .
```

## 어휘 사양(vocabulary specification) <a id="vocabulary-specification"></a>

### RDF 표기(representation)

> **EDITOR'S NOTE**

> 새로운 RDF 직렬화는 어휘가 완성되는 대로 DCAT 3에서 가용하다.

(개정된) DCAT 어휘는 RDF에서 사용할 수 있다. 주요 아티팩트 [`dcat.ttl`](https://www.w3.org/ns/dcat.ttl)은 핵심 DCAT 어휘의 직렬화이다. 그와 함께 다음을 포함한 추가 정보를 제공하는 다른 RDF 파일 세트가 있다.

1. [dcat-external.ttl](https://www.w3.org/ns/dcat-external.ttl), [dcat-external.rdf](https://www.w3.org/ns/dcat-external.rdf)와 [dcat-external.jsonld](https://www.w3.org/ns/dcat-external.jsonld) 파일에는 DCAT에서 추가 문서 또는 사용 참고 사항을 제공한 외부 정의 용어가 포함되어 있다.
2. [DCAT 버전 2](https://www.w3.org/TR/vocab-dcat-2/)에 해당하는 [dcat2.ttl](https://www.w3.org/ns/dcat2.ttl), [dcat2.rdf](https://www.w3.org/ns/dcat2.rdf)와 [dcat2.jsonld](https://www.w3.org/ns/dcat2.jsonld) 파일.
3. [DCAT의 2014 버전](https://www.w3.org/TR/vocab-dcat-1/)에 해당하는 [dcat2014.ttl](https://www.w3.org/ns/dcat2014.ttl)과 [dcat2014.rdf](https://www.w3.org/ns/dcat2014.rdf) 파일.

### 다른 어휘들의 요소 (Elements from other vocabularies)
DCAT에서는 다른 많은 어휘의 요소 사용을 필요로 한다. 또한 일반적인 [RDFS](https://www.w3.org/TR/rdf-schema/)와 [OWL2](https://www.w3.org/TR/owl2-overview/) 규칙과 패턴에 따라 외부 어휘의 요소 추가로 DCAT를 보강할 수 있다.

#### 보완 어휘(Complementary vocabularies)
더 자세한 정보를 제공하기 위해 여러 보완 어휘 요소를 DCAT와 함께 사용할 *수도* 있다. 예를 들어: [VoID 어휘](https://www.w3.org/TR/void/)의 속성은 데이터세트가 RDF 형식인 경우 DCAT-described 데이터세트에 대한 다양한 통계에 대한 기술을 허용한다. [Provenance 온톨로지](https://www.w3.org/TR/prov-o/)의 속성은 데이터세트 또는 서비스와 관련 활동과 에이전트를 생성한 워크플로우에 대한 추가 정보를 제공하는 데 사용할 수 있다. [Organization Ontology](https://www.w3.org/TR/vocab-org/)의 클래스와 속성을 사용하여 담당 에이전트의 세부 정보를 추가로 기술할 수 있다.

#### 요소 정의(Element definitions)
DCAT namespace의 외부 용어 정의(도메인과 범위 포함)는 편의를 위해서만 제공되며 규범적인 것이서는 *결코* 안된다. 이러한 용어의 공식적인 정의는 해당 사양, 즉 [Dublin Core Metadata Element Set, Version 1.1](http://dublincore.org/documents/dces/), [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/), [FOAF Vocabulary Specification 0.99 (Paddington Edition)](http://xmlns.com/foaf/spec), [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/), [RDF Schema 1.1](https://www.w3.org/TR/rdf-schema/), [SKOS Simple Knowledge Organization System Reference](https://www.w3.org/TR/skos-reference/), [W3C XML Schema Definition Language (XSD) 1.1 Part 2: Datatypes]( https://www.w3.org/TR/xmlschema11-2/)과  [vCard Ontology - for describing People and Organizations](https://www.w3.org/TR/vcard-rdf/)에 있다.


> NOTE: <a id="vocabulary-spec"></a>

>본 절의 이후 항들은 필요에 따라 그때 그때 편역하여 완성할 예정임.

## 역속성의 사용(Use of inverse property) <a id="inverse-property"></a>
[어휘 사양](#vocabulary-specification)에 기술된 속성들에 OWL 추론을 사용하지 않는 시스템에서 뿐만 아니라 상호 운용성을 보장하기 위해 의도적으로 역속성(inverse property)을 포함하지 않았다.

그러나 일부 사용 사례에 역속성이 필요하다는 것을 인식하여 DCAT은 역속성을 지원하지만 [어휘 사양](#vocabulary-specification)에 *추가로* 기술된 것에만 역속성을 사용할 수 있으며 이를 대체하는 데 사용하여서는 *결코* 안 된다는 것을 요구 사항에서 명시하고 있다.

다음 표에서 DCAT에서 지원되는 역속성을 나열한다.

| 속성 | 역속성 |
|--------|---------------|
| dcat:prev | dcat:next |
| dcat:previousVersion | dcat:nextVersion |
| dcterms:hasPart | dcterms:isPartOf |
| dcat:resource | dcat:inCatalog |
| dcterms:replaces | dcterms:isReplacedBy |
| dcterms:isReferencedBy | dcterms:references |
| dcat:hasVersion | dcat:isVersionOf |
| dcat:inSeries | dcat:seriesMember |
| foaf:primaryTopic | foaf:isPrimaryTopicOf |
| prov:wasGeneratedBy | prov:generated |

## 역참조 식별자(Dereferenceable identifier)
*이 절은 공시 표준에 포함된 것이 아니다.*

과학 및 데이터 제공자 커뮤니티에서는 출판물, 저자 및 데이터에 대해 다양한 식별자를 사용하고 있다. DCAT는 식별자를 실행 가능하게 만드는 효과적인 방법으로 주로 영구 HTTP IRI에 의존한다. 특히, 상당히 많은 식별자 체계가 역참조 가능한 HTTP IRI로 인코딩될 수 있으며, 그 중 일부는 기계 판독 가능한 메타데이터(예: DOI([Information and documentation -- Digital object identifier system](https://www.iso.org/standard/43506.html)) 및 [ORCID](https://orcid.org/))로 반환된다. 그럼에도 불구하고 데이터 제공자는 여전히 레거시 식별자, HTTP가 아닌 역참조 식별자, 로컬 발행 또는 타사 제공 식별자를 참조해야 할 수 있다. 이러한 경우 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 및 [Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/)를 사용할 수 있다.

`dcterms:identifier` 속성은 HTTP IRI와 레거시 식별자를 명시적으로 나타냅니다. 다음 예에서 `dcterms:identifier`는 데이터 세트를 식별하지만 모든 종류의 리소스와 유사하게 사용할 수 있다.

> EXAMPLE 13 

```
<https://dcat.example.org/id> a dcat:Dataset;
  dcterms:identifier "https://dcat.example.org/id"^^xsd:anyURI ;
  .
```

리소스에 HTTP 역참조 ID가 없는 경우 프록시 역참조 IRI를 사용할 수 있다. 예를 들어, [EXAMPLE 14](#ex-14)에서 `dcat.example.org/proxyid`는 `id`에 대한 프록시이다.

> EXAMPLE 14 <a id="ex-14"></a>

```
<https://dcat.example.org/proxyid> a dcat:Dataset;
  dcterms:identifier "id"^^xsd:string ;
  .
```

속성 `adms:identifier` ([Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/))는 식별자가 전역적으로 유일하고 안정적으로 존재하는 한 창작 작업의 경우 DOI, [ELI](https://eur-lex.europa.eu/eli-register/about.html), [arΧiv](https://arxiv.org/)와 저자 및 게시자와 같은 행위자를 위한 [ORCID](https://orcid.org/), [VIAF](https://viaf.org/), [ISNI](http://www.isni.org/)와 같은 다른 로컬 발행 식별자 또는 외부 식별자를 표현할 수 있다. 

[EXAMPLE 15](#ex-15)는 [`adms:schemaAgency`](https://www.w3.org/TR/vocab-adms/#schemaAgency)와 [`dcterms:creator`](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/creator)를 사용하여 식별자 체계를 정의하는 기관(예: 예에서 [DOI foundation](https://www.doi.org/))을 나타내고, `adms:schemaAgency`는 기관에 연결된 IRI가 없을 때 사용됩니다. [CrossRef](https://www.crossref.org/display-guidelines/)와 [DataCite](https://support.datacite.org/docs/datacite-doi-display-guidelines) 표시 지침은 DOI를 `https://doi.org/10.xxxx/xxxxx/` 형식의 전체 URL 링크로 표시할 것을 권장한다.

> EXAMPLE 15

```
<https://dcat.example.org/id> a dcat:Dataset;
  adms:identifier  <https://dcat.example.org/iddoi> ;
  dcterms:publisher <https://dcat.example.org/PoelenJorritH> ;
  .

<https://dcat.example.org/iddoi> a adms:Identifier ;
  # reading https://www.w3.org/TR/skos-reference/#notations more than one skos:notation can be set
  skos:notation "https://doi.org/10.5281/zenodo.1486279"^^xsd:anyURI;
  # the authority/agency defining the identifier scheme, used if the agency has no IRI
  adms:schemaAgency "International DOI Foundation" ;
  # the authority/agency defining the identifier scheme, used if the agency has IRI
  dcterms:creator  ex:InternationalDOIFundation ;
  .

ex:InternationalDOIFundation a foaf:Organization ;
  rdfs:label "International DOI Foundation" ;
  foaf:homepage <https://www.doi.org/> ;
  .

<https://dcat.example.org/PoelenJorritH> a foaf:Person;
  foaf:name "Jorrit H. Poelen" ;
  adms:identifier <https://dcat.example.org/PoelenJorritHID> ;
  .

<https://dcat.example.org/PoelenJorritHID> a adms:Identifier;
  skos:notation "https://orcid.org/0000-0003-3138-4118"^^xsd:anyURI ;
  # the authority/agency defining the identifier scheme, used if the agency has no IRI
  adms:schemaAgency "ORCID" ;
  .
```

[EXAMPLE 15](#ex-15)에서 등록자를 명명하는 것이 DOI의 철학에 어긋나기 때문에 해당 체계(예: Zenodo)를 사용하여 식별자를 할당하고 유지 관리할 책임이 있는 기관을 나타내지 않는다. DOI는 조직이 변경되거나 해당 하위 공간에 대한 책임이 다른 사람에게 이전되어도 변경되지 않는다. [EXAMPLE 15](#ex-15)는 데이터세트 생성자에 대한 로컬 발행 식별자(예: https://dcat.example.org/PoelenJorritHID)와 해당 ORCID 식별자(예: `https://orcid.org/0000-0003-3138-4118)를 보인다. 

HTTP 역참조 ID가 데이터 세트에 대한 RDF/OWL 기술을 반환할 때 `owl:sameAs` 사용을 고려할 수 있습니다. 예를 들어,

> EXAMPLE 16

```
<https://dcat.example.org/id3> a dcat:Dataset;
  ...
  owl:sameAs <https://doi.org/10.5281/zenodo.1486279> ;
  .
```

미디어 타입 `text/turtle`로 역참조될 때 `https://doi.org/10.5281/zenodo.1486279`는 데이터세트에 대한 [Schema.org](https://schema.org/)를 따르는 기술을 반환하며, 이는 `https://dcat.example.org/id`에서 제공하는 기술을 동적으로 강화할 수 있다. 

> **NOTE**:

> 데이터 세트의 식별자는 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)의 [§ 8.7 Data Identifiers](https://www.w3.org/TR/dwbp/#DataIdentifiers)의 모범 사례를 따라야 한다.

DCAT 내 데이터세트에 대한 기본 식별자와 대체(또는 레거시) 식별자를 구분해야 하는 필요성이 요구 사항으로 제기되었다. 그러나 이는 어플리케이션에 따라 다르며 일반적인 접근 방식을 요구하는 것보다 DCAT 프로필에서 더 잘 처리될 수 있다.

어플리케이션 컨텍스트에 따라 "DCAT-AP: 중복 관리 방법" 지침이 있다. 신뢰할 수 있는 데이터세트를 타 기관 카탈로그에서 수집한 데이터 세트와 구별하기 위해 채택할 수 있다.

### 공통 식별자 타입 표시(Indicating common identifier types)
식별자가 HTTP 역참조 가능하지 않은 경우 공통 식별자 타입은 상호 운용성을 위해 RDF 데이터 타입([RDF 1.1 Concepts and Abstract Syntax]( https://www.w3.org/TR/rdf11-concepts/)) 또는 사용자 지정 OWL 데이터 타입([OWL 2 Web Ontology Language Structural Specification and Functional-Style Syntax (Second Edition)](https://www.w3.org/TR/owl2-syntax/))으로 제공될 수 있다 ([EXAMPLE 17](#ex-17)의 `ex:type` 참조).

> EXAMPLE 17

```
<https://dcat.example.org/id4> a dcat:Dataset;
  ...
  adms:identifier <https://dcat.example.org/sid> .

<https://dcat.example.org/sid5> rdf:type adms:Identifier ;
  # the actual id
  skos:notation "PA 1-060-815"^^ex:type ;
  # Human readable schema agency
  adms:schemaAgency "US Copyright Office" ;
  dcterms:issued "2001-09-12"^^xsd:date ;
  .
```

등록된 IRI 타입을 사용하는 경우([Uniform Resource Identifier (URI): Generic Syntax](https://www.rfc-editor.org/rfc/rfc3986), [§ 3.1 Schema](https://www.rfc-editor.org/rfc/rfc3986#section-3.1)에 따름), 식별자 체계는 IRI의 일부이다. 따라서 '타입'에 별도의 식별자 체계를 나타내는 것은 중복되는 것이다. 예를 들어 DOI는 `info IRI scheme`[Uniform Resource Identifier (URI) Schemes](https://www.iana.org/assignments/uri-schemes/uri-schemes.xhtml)([DOI FAQ #11](https://www.doi.org/faq.html) 참조)에 네임스페이스로 등록되어 있으므로 [Uniform Resource Identifier (URI): Generic Syntax](https://www.rfc-editor.org/rfc/rfc3986)에 따라 [EXAMPLE 18](#ex-18)과 같이 인코딩해야 합니다.

> EXAMPLE 18

```
<https://dcat.example.org/sid6> rdf:type adms:Identifier ;
  # the actual id
  skos:notation "info:doi/10.1109/5.771073"^^xsd:anyURI .
```

그렇지 않으면 [DataCite schema](https://schema.datacite.org/meta/kernel-4.4/include/datacite-relatedIdentifierType-v4.xsd)([DataCite Metadata Schema](https://schema.datacite.org/))와 [FAIRsharing Registry](https://fairsharing.org/standards/?q=&selected_facets=type_exact:identifier%20schema)에 식별자 체계([arXiv](https://arxiv.org/help/arxiv_identifier) 등)에 대한 공통 타입의 예를 정의하고 있다.


## Licence and rights statements
*이 절은 공시 표준에 포함된 것이 아니다.*

> **EDITOR'S NOTE**

> 다음 참고는 아마도 DCAT3에서 삭제되어야 한다.

> **NOTE**

> DCAT 1에서 라이선스와 권리에 대한 처리는 모든 요구 사항을 충족하는 것으로 보이지 않는다([VOCAB-DCAT-1]()). 최근에 완성된 W3C ODRL 모델([ODRL-MODEL]())과 어휘([ODRL-VOCAB]())는 많은 종류의 권리와 의무를 설명하는 풍부한 언어를 제공한다. 이 절에서는 DCAT 데이터 세트 및/또는 배포에 적절한 라이선스와 권리 표현에 연결하기 위한 몇 가지 패턴을 기술한다.

리소스에 대한 액세스와 재사용 조건을 표현하는 올바른 방법을 선택하는 것은 복잡할 수 있다. 구현자는 기술한 리소스에 적용되는 조건을 결정하기 전에 항상 법률 자문을 구해야 한다.

이 사양을 세 가지 주요 상황으로 구분한다. 첫 번째는 문장에 명시적으로 '라이센스'로 선언된 리소스에 연관된 경우이다. 두 번째는 문장이 리소스의 액세스 권한만에 연관된 경우이며, 세 번째는 다른 모든 경우이다. 즉, 라이선스 조건 및/또는 액세스 권한과 관련이 없는 서술(예: 저작권 문장)이다.

> **NOTE**

> 라이선스 조건과 접근 권한 제공은 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)의 모범 사례 4("[Provide data license information](https://www.w3.org/TR/dwbp/#licenses)")과 22("[Provide an explanation for data that is not available](https://www.w3.org/TR/dwbp/#DataUnavailabilityReference)")를 각각 준수한다.

이러한 시나리오를 해결하려면 `dcterms:rights` 속성과 하위 속성 `dcterms:license`과 `dcterms:accessRights`를 사용하는 것이 바람직 하다. 더 정확하게는

1. dcterms:license를 사용하여 라이선스를 참조하시오.

> **NOTE**

> 상호 운용성을 위해 Creative Commons에서 정의한 것과 같이 잘 알려진 라이선스의 정식 IRI를 사용하는 것이 좋다.

2. `dcterms:accessRights`를 사용하여 액세스 권한에 대한 설명만 표시한다 (예: 누구나 데이터를 액세스할 수 있는지 또는 승인된 당사자만 액세스할 수 있는지 여부).

> **NOTE**

> 접근 권한을 코드 목록/분류법으로 표현할 수도 있다. [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)와 [Eprints Access Rights Vocabulary Encoding Scheme](http://purl.org/eprint/accessRights/)에서 사용되는 액세스 권한 코드 목록 [Named Authority List: Access rights](https://publications.europa.eu/en/web/eu-vocabularies/at-dataset/-/resource/dataset/access-right
)이 그 예이다.

3. 저작권 표시와 같이 `dcterms:license`와 `dcterms:accessRights`에서 다루지 않는 다른 모든 유형에 대해 `dcterms:rights`를 사용한다.

> **NOTE**

> ODRS([Open Data Rights Statement Vocabulary](http://schema.theodi.org/odrs))는 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)을 기반으로 확장된 표현 권리에 대한 보다 정교한 접근 방식을 제공하며, 이는 무엇보다도 저작권 기술과 저작권 표시를 지정하기 위한 속성을 정의한다.

> 다음 예는 공개적으로 사용 가능하고(액세스 제한 없음) 표준 라이선스, 즉 CC-BY(Creative Commons Attribution) 4.0 라이선스를 사용하여 배포되는 데이터세트에 관한 것이다. 접근 권한은 [EUV-AR] 코드 목록을 사용하여 지정되었다. `dcterms:rights` 속성은 `rdfs:label` 속성을 사용하여 텍스트 서술로 지정된 저작권 기술에 사용된다 (Dublin Core™ 사용자 가이드에 따름).

> EXAMPLE 19: 라이선스, 접근 권한 및 저작권 기술

```
ex:ds7890 a dcat:Dataset ;
# other dataset properties...
  dcterms:accessRights 
    <http://publications.europa.eu/resource/authority/access-right/PUBLIC> ;
  dcterms:rights [ a dcterms:RightsStatement ;
    rdfs:label "© 2021 ACME Inc."@en
  ] ;
  dcat:distribution [ a dcat:Distribution ;
# other distribution properties...
    dcterms:license <https://creativecommons.org/licenses/by/4.0/>
  ] ;
.
```

마지막으로, 권한이 [ODRL 정책](https://www.w3.org/TR/odrl-vocab/#term-Policy)을 통해 표현되는 특정 경우에는 
동일한 ODRL 정책 유형과 일치하는 해당 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 속성에 추가로 카탈로그된 리소스 또는 배포에 대한 기술에서 ODRL 정책으로의 링크로 `odrl:hasPolicy` 속성을 사용하는 것을 권고한다. 

> [ISSUE 1342](https://github.com/w3c/dxwg/issues/1342): DCAT에서 ODRL을 사용하는 방법

> (To be filled and edited)

> **NOTE**

> ODRL(Open Digital Rights Language)은 유연하고 상호 운용 가능한 정보 모델([ODRL Information Model 2.2](https://www.w3.org/TR/odrl-model/)), 어휘([ODRL Vocabulary & Expression 2.2.](https://www.w3.org/TR/odrl-vocab/))와 콘텐츠와 서비스의 사용(예: 권한, 금지 및 의무)에 대한 방법을 표현하기 위한 인코딩 메카니즘을 제공하는 정책 표현 언어이다.

아래 예는 매우 구체적인 사용 규칙을 갖는 데이터세트에 대해 [ODRL Vocabulary & Expression 2.2.](https://www.w3.org/TR/odrl-vocab/)를 사용하는 방법을 보여준다. 이 경우 데이터를 읽을 수 있고 파생상품을 생성할 수 있지만 데이터세트를 상업적으로 사용할 수 없다. 또한 권한을 부여하기 전에 등록해야 한다.

> EXAMPLE 20: ODRL 정첵

```
ex:ds4242 a dcat:Dataset ;
# other dataset properties...
  dcat:distribution [ a dcat:Distribution ;
# other distribution properties...
    odrl:hasPolicy [ a odrl:Policy ;
      odrl:permission [ a odrl:Permission ;
        odrl:action ( 
          <http://www.w3.org/ns/odrl/2/read>
          <http://www.w3.org/ns/odrl/2/derive> 
        ) 
      ];
      odrl:prohibition [ a odrl:Prohibition ;
        odrl:action <http://creativecommons.org/ns#CommercialUse>
      ] ;
      odrl:obligation [ a odrl:Duty ;
        odrl:action <https://schema.org/RegisterAction> 
      ];
    ] ;
  ] ;
.
```

위의 예는 ODRL 자산을 명시적으로 정의하지 않으므로 [ODRL Information Model 2.2](https://www.w3.org/TR/odrl-model/)의 [§ 2.2.3 Target Policy Property](https://www.w3.org/TR/odrl-model/#policy-has)에 따라 둘러싸는 식별된 엔터티가 정책의 대상이라고 가정한다. 또한 위의 예는 [ODRL Information Model 2.2](https://www.w3.org/TR/odrl-model/)의 [§ 2.7.1 압축 정책](https://www.w3.org/TR/odrl-model/#composition-compact)에 따라 ODRL *압축 정책 규칙(을 따른다.

## 시공간 (Time and space)
*이 절은 공시 표준에 포함된 것이 아니다.*

### 시간 속성(Temporal properties)
DCAT를 사용하여 자원의 다섯 가지 시간 속성을 기술할 수 있다.

- [`dcterms:issued`](https://w3c.github.io/dxwg/dcat/#Property:resource_release_date)를 사용하여 자원의 릴리스 시각을 제공한다. 통상적으로 [`xsd:date`](https://www.w3.org/TR/xmlschema11-2/#date)로 그 값을 인코딩한다.
- [`dcterms:modified`](https://w3c.github.io/dxwg/dcat/#Property:resource_update_date)를 사용하여 리소스의 개정 또는 업데이트 시각을 표시한다. 통상적으로 [`xsd:date`](https://www.w3.org/TR/xmlschema11-2/#date)로  그 값을 인코딩한다.
- [`dcterms:accrualPeriodicity`](https://w3c.github.io/dxwg/dcat/#Property:dataset_frequency)를 사용하여 자원의 업데이트 일정은 표시한다. 그 값은 [Dublin Core Collection Description Frequency Vocabulary](http://www.dublincore.org/specifications/dublin-core/collection-description/frequency/)와 같은 통제된 어휘에서 가져와야 한다.
- [`dcat:temporalResolution`](https://w3c.github.io/dxwg/dcat/#Property:dataset_temporal_resolution)을 사용하여 데이터세트내 항목의 최소 시간을 제공한다. [`xsd:duration`](https://www.w3.org/TR/xmlschema11-2/#duration)으로 그 값은 인코딩한다. 업데이트 일정과 시간 해상도를 결합하여 아래 예에서와 같이 다양한 종류의 시계열 데이터에 대한 기술을 지원할 수 있다.
- [`dcterms:temporal`](https://w3c.github.io/dxwg/dcat/#Property:dataset_temporal)을 사용하여 데이터세트의 시간 범위를 제공한다. 값은 [`dcterms:PeriodOfTime`](http://purl.org/dc/terms/PeriodOfTime)이다. `dcterms:PeriodOfTime`의 세부사항을 표현하기 위한 여러 옵션의 사용을 [6.15 클래스: 기간](?)에서 권장하고 았다. 이들의 예는 다음과 같다.

> EXAMPLE 21: 매일 게시되는 15분 시계열

```
ex:ds913
  a dcat:Dataset ;
  dcterms:accrualPeriodicity <http://purl.org/cld/freq/daily> ;
  dcat:temporalResolution "PT15M"^^xsd:duration ;
.
```

> EXAMPLE 22: 즉시 게시되는 15분 시간별 데이터

```
ex:ds782
  a dcat:Dataset ;
  dcterms:accrualPeriodicity <http://purl.org/cld/freq/continuous> ;
  dcat:temporalResolution "PT1H"^^xsd:duration ;
.
```

> EXAMPLE 23: 단힌 간격의 시간 범위 <a id="ex-23"></a>

```
ex:ds257 a dcat:Dataset ;
  dcterms:temporal [ a dcterms:PeriodOfTime ;
    dcat:startDate "2016-03-04"^^xsd:date ;
    dcat:endDate   "2018-08-05"^^xsd:date ;
  ] .
```
다음 데이터세트 사양은 [EXAMPLE 23](#ex-23)의 사양과 동일하지만 [Time Ontology in OWL](https://www.w3.org/TR/owl-time/)을 사용한다.

> EXAMPLE 24: `time:ProperInterval`을 사용한 닫힌 간격의 시간 범위

```
ex:ds348 a dcat:Dataset ;
  dcterms:temporal [ a dcterms:PeriodOfTime , time:ProperInterval ;
    time:hasBeginning [ a time:Instant ;
      time:inXSDDate "2016-03-04"^^xsd:date ;
    ] ;
    time:hasEnd [ a time:Instant ;
      time:inXSDDate "2018-08-05"^^xsd:date ;
    ] ;
  ] .
```

> EXAMPLE 25: `gYear`를 사용한 적절한 간격으로서 시간 범위

```
ex:ds429 a dcat:Dataset ;
  dcterms:temporal [ a dcterms:PeriodOfTime , time:ProperInterval ;
    time:hasBeginning [ a time:Instant ;
      time:inXSDgYear "1914"^^xsd:gYear ;
    ] ;
    time:hasEnd [ a time:Instant ;
      time:inXSDgYear "1939"^^xsd:gYear ;
    ] ;
  ] .
```

> EXAMPLE 26: 지질 데이터세트를 위한 시간 범위

```
ex:ds850 a dcat:Dataset ;
  dcterms:temporal [ a dcterms:PeriodOfTime , time:ProperInterval ;
    time:hasBeginning [ a time:Instant ;
      time:inTimePosition [ a time:TimePosition ;
        time:hasTRS <http://resource.geosciml.org/classifier/cgi/geologicage/ma> ;
        time:numericPosition "541.0"^^xsd:decimal ;
      ] ;
    ] ;
    time:hasEnd [ a time:Instant ;
      time:inTimePosition [ a time:TimePosition ;
        time:hasTRS <http://resource.geosciml.org/classifier/cgi/geologicage/ma> ;
        time:numericPosition "251.902"^^xsd:decimal ;
      ] ;
    ] ;
  ] .
```

> EXAMPLE 27: 열린 간격의 시간 범위(끝 시각 없음)

```
ex:ds127 a dcat:Dataset ;
  dcterms:temporal [ a dcterms:PeriodOfTime ;
    dcat:startDate "2016-03-04"^^xsd:date ;
  ] .
```

> EXAMPLE 28: 열린 간격의 시간 범위(시작 시각 없음)

```
ex:ds586 a dcat:Dataset ;
  dcterms:temporal [ a dcterms:PeriodOfTime , time:ProperInterval ;
    time:hasEnd [ time:inXSDDate "2018-08-05"^^xsd:date ] ;
  ] .
```

### 공간 속성(Spatial properties)
DCAT을 사용하여 데이터세트의 두 가지 공간 속성을 기술할 수 있다.

1. [`dcat:spatialResolutionInMeters`](https://w3c.github.io/dxwg/dcat/#Property:dataset_spatial_resolution)를 사용하여 데이터세트 항목의 최소 공간 단위를 표시한다. 값은 십진수이다.
<br>[`dcat:spatialResolutionInMeters`](https://w3c.github.io/dxwg/dcat/#Property:dataset_spatial_resolution)의 사용 예는 [EXAMPLE 3](#ex-3)에 있다.

2. [`dcterms:spatial`](https://w3c.github.io/dxwg/dcat/#Property:dataset_spatial)을 사용하여 데이터세트의 공간 범위를 표시한다. 값은 [`dcterms:Location`](http://purl.org/dc/terms/Location)이다. `dcterms:Location`의 세부사항을 표현하기 위한 여러 옵션을 [6.16 Class: Location](?)에서 권장하고 있다.

이들의 예는 다음과 같다.

> **NOTE**

> 다음 예들은 [Spatial Data on the Web Best Practices](https://www.w3.org/TR/sdw-bp/)에 포함된 관련 예제를 기반으로 한다 (특히 [§ 12.2.2 기하학 및 좌표 참조 시스템](https://www.w3.org/TR/sdw-bp/#geometry-and-crs)).

> 예에서 `locn:geometry`, `dcat:bbox` 및 `dcat:centroid` 속성의 경우 기하학은 항상 WKT로 지정된다. [GeoSPARQL - A Geographic Query Language for RDF Data](http://www.opengeospatial.org/standards/geosparql)에 따라 CRS 사양이 생략되면 기본 CRS, 즉 CRS84(WGS84에 해당하지만 축 순서는 경도/위도임)가 사용됨을 의미한다.

> 좌표 참조 시스템과 기하학 인코딩에 대한 자세한 내용은 [Spatial Data on the Web Best Practices](https://www.w3.org/TR/sdw-bp/), 특히 다음을 참조하시오.

> [§ 9 좌표 참조 시스템(CRS)](https://www.w3.org/TR/sdw-bp/#CRS-background)
> [§ 12.2.2 기하학 및 좌표 참조 시스템](https://www.w3.org/TR/sdw-bp/#geometry-and-crs)

공간 범위가 암스테르담에 있는 Anne Frank의 집에 해당하는 데이터세트로, 다각형으로 지정되었다 (좌표 참조 시스템은 CRS84임).

> EXAMPLE 29: 다각형인 공간 범위 <a id="ex-29"></a>

```
<AnneFrank_0> a dcat:Dataset ;
  dcterms:spatial [
    a dcterms:Location ;
    locn:geometry """POLYGON ((
      4.8842353 52.375108 , 4.884276 52.375153 ,
      4.8842567 52.375159 , 4.883981 52.375254 ,
      4.8838502 52.375109 , 4.883819 52.375075 ,
      4.8841037 52.374979 , 4.884143 52.374965 ,
      4.8842069 52.375035 , 4.884263 52.375016 ,
      4.8843200 52.374996 , 4.884255 52.374926 ,
      4.8843289 52.374901 , 4.884451 52.375034 ,
      4.8842353 52.375108
    ))"""^^geosparql:wktLiteral ;
  ] .
```

![Fig. 2](images/ex-spatial-coverage-geometry-anne-frank-house.png)
<center>Fig. 2 *지오메트리로 표시된 공간 범위의 지도 미리보기*</center> <a id="fig-2"></a>

[EXAMPLE 29](#ex-2)와 동일한 데이터세트이지만 다각형의 좌표는 네덜란드 국가 CRS - [EPSG:28992](https://epsg.org/crs_28992/Amersfoort-RD-New.html)("Amersfoort / RD New")를 사용하여 지정하였다.

> EXAMPLE 30: 특정한 CRS를 사용한 다각형인 공간 범위 <a id="ex-30"></a>

```
<AnneFrank_1> a dcat:Dataset ;
  dcterms:spatial [
    a dcterms:Location ;
    locn:geometry """<http://www.opengis.net/def/crs/EPSG/0/28992> POLYGON ((
      120749.725 487589.422 , 120752.55 487594.375  ,
      120751.227 487595.129 , 120732.539 487605.788 ,
      120723.505 487589.745 , 120721.387 487585.939 ,
      120740.668 487575.07  , 120743.316 487573.589 ,
      120747.735 487581.337 , 120751.564 487579.154 ,
      120755.411 487576.96  , 120750.935 487569.172 ,
      120755.941 487566.288 , 120764.369 487581.066 ,
      120749.725 487589.422
    ))"""^^geosparql:wktLiteral ;
  ] .
```

[EXAMPLE 29](#ex-2)와 동일한 데이터셋이지만 Anne Frank 집의 중심점/대표점을 사용하여 공간 커버리지를 지정하였다.

> EXAMPLE 31: 중심점으로 표시한 공간 범위

```
<AnneFrank_2> a dcat:Dataset ;
  dcterms:spatial [
    a dcterms:Location ;
    dcat:centroid "POINT(4.88412 52.37509)"^^geosparql:wktLiteral ;
  ] .
```

![Fig. 3](images/ex-spatial-coverage-centroid-anne-frank-house.png)
<center>Fig. 3 *중심점으로 표시된 공간 범위의 지도 미리보기*</center> <a id="fig-3"></a>

공간 범위(네덜란드)가 바운딩 박스(bounding box)로 표시된 네덜란드 우편 주소 데이터세트이다.

> EXAMPLE 32: 바운딩 박스로 표시한 공간 범위

```
ex:Dutch-postal a dcat:Dataset ;
  dcterms:title "Adressen"@nl ;
  dcterms:title "Addresses"@en ;
  dcterms:description """INSPIRE Adressen afkomstig uit de basisregistratie Adressen,
                   beschikbaar voor heel Nederland"""@nl ;
  dcterms:description """INSPIRE addresses derived from the Addresses base registry,
                   available for the Netherlands"""@en ;
  dcat:theme <http://inspire.ec.europa.eu/theme/ad> ;
  dcterms:spatial [
    a dcterms:Location ;
    dcat:bbox """POLYGON((
      3.053 47.975 , 7.24  47.975 ,
      7.24  53.504 , 3.053 53.504 ,
      3.053 47.975
    ))"""^^geosparql:wktLiteral ;
  ] .
```

![Fig. 4](images/ex-spatial-coverage-bbox-netherlands.png)
<center>Fig. 4 *바운딩 박스로 표시된 공간 범위의 지도 미리보기*</center> <a id="fig-4"></a>

## 버전 관리(Versioning) <a id="versioning"></a>
*이 절은 공시 표준에 포함된 것이 아니다.*

> **EDITOR'S NOTE**

> 이는 DCAT의 버전 관리 지원에 대한 논의를 위한 새로운 초안이다.

> [Data Catalog Vocabulary (DCAT) - Version 3](https://www.w3.org/TR/2020/WD-vocab-dcat-3-20201217/)의 첫 번째 초안과 비교하여 이 절은 특히 리소스 개정(revision)에서 파생된 버전에 중점을 두고 버전 체인과 계층 구조(이전, 다음, 현재, 마지막 버전)의 사양에 대한 [PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/) 접근 방식을 따르도록 수정되었다. 

> 특히, 소개 글은 새로운 범위에 따라 수정되었으며, 버전 타입은 제거되었으며, 원래 [Version information](#version-info)에 포함된 이전 버전과의 호환성을 포함하여 버전 간의 관계를 지정하는 방법을 설명하는 새로운 절이 추가되었다. 

> 더우기, OWL과 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)에서 사용된 것과 DCAT 버전 관리 접근 방식을 비교하기 위해 끝에 [새로운 절](#complementary-approach)이 추가되었다.

> 다른 절에는 편집 변경 사항만 포함되었다.

> **주의**: 이 절에서 설명하는 모든 속성은 아직 DCAT의 RDF 정의에 추가되지 않았다.

*버전*의 개념은 리소스와 파생된 리소스 간의 일종의 관계를 나타내는 일반적인 용어로 자주 사용된다. 예를 들면 개정, 판, 각색, 번역 등이 있다.

이 절은 개정으로 결과인 버전을 기술하기 위해 DCAT을 사용하는 방법에 중점을 둔다.

이를 위해 DCAT은 기존 어휘, 특히 [PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/) 온톨로지의 버전 구성 요소와 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/), [OWL 2 Web Ontology Language Document Overview (Second Edition)](https://www.w3.org/TR/owl2-overview/)
) 및 [Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/)의 관련 용어에 기반을 둔다.

버전닝은 카탈로그, 카탈로그 레코드, 데이터 세트, 배포를 포함한 모든 일급 시민 DCAT 리소스에 적용될 수 있다는 점에 유의하는 것이 중요하다.

또한 다음 절에서 설명하는 DCAT 접근 방식은 특정 타입의 리소스(예: [OWL 2 Web Ontology Language Document Overview (Second Edition)](https://www.w3.org/TR/owl2-overview/)는 온톨로지에 대한 버전 속성 집합을 제공함) 뿐만 아니라 주어진 도메인 및 커뮤니티에사 사용되는 방식에 보완의 의미를 지니고 있다. DCAT 버전 관리 접근 방식과 다른 어휘의 접근 방식의 비교는 [버전닝에 대한 보완 접근 방식](#complementary-approach)을 참조하시오.

> **NOTE**

> 버전 개념은 커뮤니티 관행, 데이터 관리 정책 및 워크플로와 매우 관련이 있다. 새 버전을 출시해야 하는 시기와 이유를 결정하는 것은 데이터 제공자의 몫이다. 이러한 이유로 DCAT는 리소스의 변경 사항이 새 릴리스로 전환되어야 하는 시기에 대한 정의 또는 규칙을 제공하는 것을 지양하고 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)(8.6 데이터 버전 관리 및 8.7 데이터 식별자)에 대한 지침을 참조한다.

### 버전간의 관계(Relationships between versions) <a id="relationships-between-versions"></a>
DCAT는 버전 간의 다음과 같은 관계를 지원한다.

1. 버전 체인과 계층 구조(버전 이력)를 나타낸다.
2. 한 버전이 다른 버전으로 대체되었는지 여부를 나타낸다.

#### 버전 체인과 계층(Version chains and hierarchies) <a id="version-chains-and-hierarchies"></a>
DCAT는 해당 [PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/) 속성과 정렬된 버전 기록을 설명하기 위한 특정 속성을 정의한다.

- [`dcat:previousVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_previous_version)([`pav:previousVersion`](https://pav-ontology.github.io/pav/#d4e459)와 동일)
- [`dcat:hasVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_has_version)([`pav:hasVersion`](https://pav-ontology.github.io/pav/#d4e395)와 동일);
- [`dcat:hasCurrentVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_has_current_version)([`pav:hasCurrentVersion`](https://pav-ontology.github.io/pav/#d4e359)와 동일하며 [`dcat:hasVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_has_version)의 하위 속성임).

> **EDITOR'S NOTE**

> 이전 작업 초안 [Data Catalog Vocabulary (DCAT) - Version 3](https://www.w3.org/TR/2020/WD-vocab-dcat-3-20201217/)]은 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 및 [Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/)의 유사한 속성, 즉 다음을 사용하고 있었다.

> -  버전 계층의 경우 `dcterms:hasVersion` / `dcterms:isVersionOf`
> - 버전 체인과 데이터세트 시리즈 모두에 대해 `adms:prev`, `adms:next`와 `adms:last` 

> 그러나 이들의 정의와 사용은 [PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/)의 유사한 속성과 완전히 일치하지는 않는다. 예:

> - [`dcterms:hasVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_has_version)은 "기술된 자원의 버전, 에디션 또는 개작(adaptation)인 관련 자원으로 정의되어 더 넓은 의미에서 버전의 개념을 사용하는 반면 [PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/)는 개정(revision)의 결과로 버전을 구체적으로 서술한다.
> - [`adms:last`](https://www.w3.org/TR/vocab-adms/#adms-last)를 자산의 현재 또는 최신 버전에 대한 링크로 정의하는 반면, [PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/)에서는 리소스의 현재와 최신 버전이 다를 수 있고 다른 속성을 사용하여 연결될 수 있다.

> 더우기, [Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/) 속성의 정의는 "버전"에 사용되도록 명시적으로 명시되어 있지만, [XHTML Vocabulary]https://www.w3.org/1999/xhtml/vocab()에서는 해당 용어의 하위 속성이며, 여기서 컬렉션(예: 웹 사이트의 페이지 집합)내 리소스를 연결하는 데 사용된다. 예를 들어 `xhv:last`는 "last는 리소스 컬렉션에서 마지막 리소스이다"를 정의한다.

> 이를 기반으로 후속 초안(draft)에서는 [PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/)와 같은 속성이 버전에 사용되는 반면 [Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/) 속성과 `dcterms:hasVersion` 또는 `dcterms:isVersionOf` 속성을 사용하지 않는다.

`dcat:previousVersion` 속성은 주어진 버전에서 첫 번째 이전 버전으로 이동할 수 있는 버전 체인을 구축하는 데 사용된다. 이는 가장 일반적인 사용 사례를 반영한 것이다. 즉, 카탈로그에서 고유한 리소스로 게시된 서로 다른 버전을 연결한다.

이 외에도 `dcat:hasVersion` 속성을 사용하여 추상 리소스를 해당 버전에 연결하여 버전 계층을 지정할 수 있다.

필요한 경우 특정 속성으로 버전 계층을 추가로 기술할 수 있다. 더 정확하게 `dcat:hasCurrentVersion` 속성은 현재 버전의 콘텐츠에 해당하는 스냅샷에 추상 리소스를 연결하는 반면 `dcat:isVersionOf` 속성(`dcat:hasVersion`의 역)은 한 버전에서 추상 리소스(이 속성의 사용은 대해서는 [역속성의 사용](#inverse-property)을 참조)로의 역 링크를 지정할 수 있다.

> **NOTE**

> DCAT를 사용하여 리소스 상태를 지정하는 방법은 [리소스 수명 주기](#resource-life-cycle)를 참조하시오.

버전 체인과 계층 구조를 지정하는 데 필요한 유일한 속성은 각각 `dcat:previousVersion`과 `dcat:hasVersion`이다. 다른 것을 사용할지 여부는 해당 사용 사례의 요구 사항에 따라 다를 수 있다.

다음 예는 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)의 [§ 8.6 데이터 버전 관리](https://www.w3.org/TR/dwbp/#dataVersioning)에 있는 내용을 재사용하고 이 절에서 설명하는 속성을 사용하여 버스 정류장 데이터세트에서 버전 체인과 계층 구조를 지정하는 방법을 보여주기 위해 수정하였다.

MyCity 버스 정류장 데이터세트는 버스 정류장 목록이 변경될 때마다 업데이트된다. 주어진 시각에 버스 정류장에 대한 역사적 기록을 유지하기 위해 다른 버전이 보존된다. 추상 데이터 세트는 최신 버스 정류장 목록이 있는 버전을 가리킨다.

[Fig. 5](#fig-5)에서 IRI `mycity-bus:stops`가 있는 리소스는 항상 최신 버전의 MyCity 버스 정류장 데이터세트에 해당하는 추상 리소스이고 다른 것들은 특정 버전에 해당된다. 현재 버전은 IRI `mycity-bus:stops-2015-12-07` 버전이다.

![Fig. 5](images/version-chain-and-hierarchy.svg)
<center>Fig. 5 MyCity 버스 정류장 데이터세트의 체인과 계층</center> <a id="fig-5"></a>

> EXAMPLE 33: MyCity 버스 정류장 데이터세트의 버전 이력 <a id="ex-33"></a>

```
mycity-bus:stops a dcat:Dataset ;
  ...
  dcat:hasVersion mycity-bus:stops-2015-01-01 ,
    mycity-bus:stops-2015-05-05 ,
    mycity-bus:stops-2015-12-07 ;
  dcat:hasCurrentVersion mycity-bus:stops-2015-12-07 ;
.

mycity-bus:stops-2015-01-01 a dcat:Dataset ;
  ...
  dcat:isVersionOf mycity-bus:stops ;
.

mycity-bus:stops-2015-05-05 a dcat:Dataset ;
  ...
  dcat:previousVersion mycity-bus:stops-2015-01-01 ;
  dcat:isVersionOf mycity-bus:stops ;
.

mycity-bus:stops-2015-12-07 a dcat:Dataset ;
  ...
  dcat:previousVersion mycity-bus:stops-2015-05-05 ;
  dcat:isVersionOf mycity-bus:stops ;
.
```

#### 다른 버전으로 대체된 버전(Versions replaced by other ones)
또 다른 타입의 관계는 주어진 버전이 다른 버전을 대체하는지 여부에 관한 것이다. 이를 위해 DCAT은 백 링크가 제공되어야 하는 경우 관련 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 속성, 즉 [`dcterms:replaces`](https://w3c.github.io/dxwg/dcat/#Property:resource_replaces)와 역 dcterms:isReplacedBy를 재사용한다.

이러한 속성이 그 자체로 버전 체인을 나타내지 않는다는 점은 주목할 가치가 있다. 즉, 버전이 바로 이전 버전을 반드시 대체하지는 않는다.

다음 예는 [EXAMPLE 33](#ex-33)의 MyCity 버스 정류장 데이터세트에 대한 기술을 재사용하여 DCAT에서 대체된 버전을 지정하는 방법을 보여준다.

> EXAMPLE 34: MyCity 버스 정류장 데이터세트의 대체 버전

```
mycity-bus:stops-2015-01-01 a dcat:Dataset ;
  ...
  dcat:isVersionOf mycity-bus:stops ;
.

mycity-bus:stops-2015-05-05 a dcat:Dataset ;
  ...
  dcat:previousVersion mycity-bus:stops-2015-01-01 ;
  dcat:isVersionOf mycity-bus:stops ;
  dcterms:replaces mycity-bus:stops-2015-01-01 ;
.

mycity-bus:stops-2015-12-07 a dcat:Dataset ;
  ...
  dcat:previousVersion mycity-bus:stops-2015-05-05 ;
  dcat:isVersionOf mycity-bus:stops ;
  dcterms:replaces mycity-bus:stops-2015-05-05 ;
```

### Version information <a id="version-info"></a>

> **EDITOR'S NOTE**

> 이 초안은 DCAT 네임스페이스에 정의되고 [버전 체인과 계층](#version-chains-and-hierarchies)에 설명된 버전 속성을 보완하기 위해 첫 번째 초안의 `owl:versionInfo` 대신 사용할 새로운 속성 `dcat:version`의 정의를 제안한 것이다 (Issue [#1280](https://github.com/w3c/dxwg/issues/1280) 참조).

> 또한 버전 간 하위 호환성 사양에 대한 지원을 제거하였다 (Issue [#1258](https://github.com/w3c/dxwg/issues/1258) 참조).

이전 절에서 설명한 관계 외에도 버전이 지정된 리소스는 예를 들어 원래 리소스와의 차이(버전 "델타"), 버전 식별자와 릴리스 날짜를 설명하는 추가 정보와 연관될 수 있다.

이러한 목적을 위해 DCAT은 다음 속성을 사용합니다.

- 버전 이름  또는 식별자를 위한,[`dcat:version`](https://w3c.github.io/dxwg/dcat/#Property:resource_version)([`pav:version`](https://pav-ontology.github.io/pav/#d4e395)([PAV - Provenance, Authoring and Versioning. Version 2.3.1](https://pav-ontology.github.io/pav/)와 동일))
- 버전 릴리스 날짜를 위한 [`dcterms:issued`](https://w3c.github.io/dxwg/dcat/#Property:resource_release_date) ([DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/))
- 리소스의 이전 버전과의 역호환성 문제를 포함하여 변경 사항에 대한 기술을 위한 [`adms:versionNotes`]([Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/))

> **NOTE**: 

> DCAT는 버전 이름/식별자를 지정하는 방법을 규정하지 않으며 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)의 [모범 사례 7: 버전 식별자 제공](https://www.w3.org/TR/dwbp/#VersioningInfo)에 대한 지침을 참조한다.

버전 정보를 DCAT에서 지정할 수 있는 방법을 보이기 위해 다음 예예서 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)의 [모범 사례 7: 버전 식별자 제공](https://www.w3.org/TR/dwbp/#VersioningInfo)의 버전 시별자를 재사용한다.

> EXAMPLE 35: MyCity 버스 정류장 데이터세트의 첫 버전

```
mycity-bus:stops-2015-01-01
  a dcat:Dataset ;
  dcterms:title "Bus stops of MyCity" ;
  ...
  dcterms:issued "2015-01-01"^^xsd:date ;
  ...
  dcat:version "1.0" ;
  adms:versioNotes "First version of the bus stop dataset."@en ;
.
```

### 리소스 수명 주기(Resource life-cycle) <a id="resource-life-cycle"></a>
리소스의 수명 주기는 버전 관리와 직교하는 측면이며, 때로는 직접적으로 관련이 있다. 항상 그런 것은 아니지만 (예: 승인 워크플로가 있는 경우, 수정이 필요하지 않으면 리소스가 변경되지 않을 수 있다.) 수명 주기(개념에서 생성과 게시까지)에 따라 리소스가 진화하면 새 버전이 생성될 수 있다. 유사하게, 새 버전의 생성은 반드시 상태의 변경으로 이어지지 않을 수도 있다 (예: 변경 사항이 중요하지 않거나 아직 개발 중인 리소스를 구현하는 경우). 또한 개정(오류 수정, 새로운 콘텐츠 추가 등)으로 인해 리소스가 교체되는 경우 다른 수명 주기 상태(예: 폐기 또는 철회)로 이동할 수 있다.

리소스의 수명 주기와 관련된 상태는 데이터 공급자와 데이터 소비자 모두의 관점에서 그 자체로 중요한 정보인 경우가 흔히 있다. 데이터 소비자의 경우 리소스가 아직 개발 중인지 여부는 물론 더 이상 사용되지 않거나 철회되었는지 여부 (이러한 경우 사용할 새 버전이 있는지 여부)를 아는 것이 중요하다. 반면에 데이터 공급자의 경우 수명 주기의 상태로 리소스에 플래그를 지정하는 것은 데이터 관리 워크플로의 올바른 관리를 위한 기본이다. 예를 들어 게시되기 전 리소스가 안정적이어야 하고 승인 및/또는 등록된 것으로 표시되어야 할 수 있다. 마지막으로 리소스의 실제 상태 외에 또 다른 유용한 정보는 리소스가 다른 상태(예: 생성, 검토, 수락, 게시)로 이동한 경우이다.

버전 관리의 경우 리소스 수명 주기는 커뮤니티 관행, 데이터 관리 정책 및 워크플로에 따라 다르다. 또한 다른 리소스 타입 (예: 데이터세트와 카탈로그 레코드)은 수명 주기 상태가 다를 수 있다.

수명 주기 상태를 지정하기 위해 DCAT는 적절한 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)의 시간 관련 속성(`dcterms:created`, `dcterms:dateSubmitted`, `dcterms:dateAccepted`, `dcterms:dateCopyrighted`, [`dcterms:issued`](https://w3c.github.io/dxwg/dcat/#Property:resource_release_date), [`dcterms:modified`](https://w3c.github.io/dxwg/dcat/#Property:resource_update_date), `dcterms:valid`)과 함께 `adms:status`([Asset Description Metadata Schema (ADMS)](https://www.w3.org/TR/vocab-adms/)) 속성을 사용한다. 그러나 DCAT에서는 특정 수명 주기 상태 집합의 사용을 규정하지 않고 관련 어플리케이션 시나리오에 적합한 기존 표준과 커뮤니티 관행을 참조하도록 한다.

> **NOTE**

> 수명 주기 상태의 예는 다음과 같습니다.

> - 항목 등록에 대한 ISO 표준([Geographic information -- Procedures for item registration]( https://www.iso.org/standard/32553.html))에 정의된 항목(accepted / not accepted, deprecated, experimental, reserved, retired, stable, submitted, superseded, valid / invalid).
> - [Geographic information -- Metadata](https://www.iso.org/standard/26020.html)에 정의된 진행 코드(accepted, completed, deprecated, final, historical archive, not accepted, obsolete, ongoing, pending, planned, proposed, required, retired, superseded, tentative, under development, valid, withdrawn) .
> - [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)에서 사용되는 ADMS 상태 용어 [Joinup. ADMS Controlled Vocabularies](https://joinup.ec.europa.eu/svn/adms/ADMS_v1.00/ADMS_SKOS_v1.00.html는 completed, deprecated, under development와 withdrawn의 네 가지 상태를 포함한다.
> - EU 어휘 레지스트리의 데이터세트 상태 [Named Authority List: Dataset statuses](https://publications.europa.eu/en/web/eu-vocabularies/at-dataset/-/resource/dataset/dataset-status)와 개념 상태 [Named Authority List: Concept statuses](https://publications.europa.eu/en/web/eu-vocabularies/at-dataset/-/resource/dataset/concept-status) 어휘.

> UK Government Linked Data Registry 프로젝트([UKGOVLD-REG]())는 상태 전환 기준과 함께 [Geographic information -- Procedures for item registration](https://www.iso.org/standard/32553.html)에 정의된 수명 주기 상태의 레지스트리에서 사용 방법에 대한 예를 제공하고 있다.

### 버전 관리에 대한 보완적 접근(Complementary approaches to versioning) <a id="complementary-approach"></a>
DCAT 버전 관리 방식은 특정 커뮤니티, 도메인 및 리소스 타입에서 사용되는 기존 버전 관리 방식과 공존할 수 있다.

예를 들어, 다음 표는 DCAT 버전 속성과 유사한 개념을 지정하는 데 가장 자주 사용되는 어휘, 즉 온톨로지, [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 및 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)에 대한 OWL 간의 대응을 보인다.

| DCAT | OWL | DCMI Metadata Terms | PROV-O: The PROV Ontology |
|------|-----|---------------------|---------------------------|
| [`dcat:hasVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_has_version) | | [`dcterms:hasVersion`](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/hasVersion) | [`prov:generalizationOf`](https://www.w3.org/TR/prov-o/#generalizationOf) |
| [`dcat:isVersionOf`](https://w3c.github.io/dxwg/dcat/#Property:resource_is_version_of) | | [`dcterms:isVersionOf`](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/isVersionOf) | [`prov:specializationOf`](https://www.w3.org/TR/prov-o/#specializationOf) |
| [`dcat:hasCurrentVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_has_current_version) | owl:versionIRI | | |	
| [`dcat:previousVersion`](https://w3c.github.io/dxwg/dcat/#Property:resource_previous_version) | owl:priorVersion | | [`prov:wasRevisionOf`](https://www.w3.org/TR/prov-o/#wasRevisionOf) |
| [`dcat:version`](https://w3c.github.io/dxwg/dcat/#Property:resource_version) | owl:versionInfo | | | 

대응이 동등함을 의미하지는 않습니다. 이러한 속성은 범위와 의미가 다르기 때문에 서로 보완할 수는 있지만 대체할 수는 없다. 특히, OWL 속성은 `owl:Ontology`로 타입이 지정될 수 있는 리소스에 사용되는 반면 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 속성은 매우 광범위한 버전 개념(edition과 adaptation 포함)을 사용한다. 반면에 DCAT 버전 관리 속성은 카탈로그의 모든 리소스에서 사용하도록 되어 있으며 [버전 관리](#versioning) 소개에서 설명한 대로 매우 구체적인 *버전* 개념을 사용한다. 마지막으로 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/) 속성 `prov:wasRevisionOf`는 의미상 `dcat:previousVersion`과 유사하지만 버전 체인을 구축하는 데 명시적으로 사용되지 않는 반면 `prov:generalizationOf`와 `prov:specializationOf`는 의미상 하위 속성 각각 `dcat:hasVersion`과 `dcat:isVersionOf` 보다 넓은 의미를 갖는다. 

다음 예는 DCAT과 OWL을 버전 관리 [Data Catalog Vocabulary (DCAT) - Version 2](https://www.w3.org/TR/vocab-dcat-2/)에 보완적으로 사용할 수 있는 방법을 보인다.

> EXAMPLE 36: DCAT 3 버전 관리를 위한 DCAT과 OWL의 사용

```
<http://www.w3.org/ns/dcat> a owl:Ontology , dcat:Dataset ;
  owl:versionInfo "3" ;
  dcat:version "3" ;
  owl:versionIRI <http://www.w3.org/ns/dcat3> ;
  dcat:hasCurrentVersion <http://www.w3.org/ns/dcat3> ;
  owl:priorVersion <http://www.w3.org/ns/dcat2> ;
  dcat:previousVersion <http://www.w3.org/ns/dcat2> ;
  dcat:hasVersion <http://www.w3.org/ns/dcat2014> ,
    <http://www.w3.org/ns/dcat2> ,
    <http://www.w3.org/ns/dcat3> ;
.
```


## 데이터세트 시리즈(Dataset series)
*이 절은 공시 표준에 포함된 것이 아니다.*

> **EDITOR'S NOTE**

> 이것은 DCAT에서 데이터세트 시리즈(dataset series) 지원에 대한 논의하는 새로운 초안이다. [VData Catalog Vocabulary (DCAT) - Version 3](https://www.w3.org/TR/2020/WD-vocab-dcat-3-20201217/)의 첫 번째 초안을 수정한 것이다.

> Issue [#1272](https://github.com/w3c/dxwg/issues/1272)에서 제안된 새로운 클래스 `dcat:DatasetSeries`를 정의한다.

> 하위 데이터세트를 데이터세트 시리즈에 연결하는 속성 `dcat:inSeries` 하나만 정의한다. 새로운 속성 `dcat:inSeries`는 `dcterms:isPartOf`를 대체하여 데이터세트 시리즈의 구성과 [느슨하게 구조화된 카탈로그](https://www.w3.org/TR/vocab-dcat-3/#examples-bag-of-files)를 구별할 수 있도록 한다 (Issue [#1307](https://github.com/w3c/dxwg/issues/1307) 참조). 마지막으로 속성 `adms:prev`, `adms:next` 및 `adms:last`가 새로 정의된 속성인 `dcat:prev`, `dcat:next` 및 `dcat:last`로 대체되었다 (Issue [#1308](https://github.com/w3c/dxwg/issues/1308) 참조).

"데이터세트 시리즈"는 어떻게든 상호 연관되어 별도로 게시된 데이터를 나타낸다. 예를 들어 단일 데이터세트로 생성하는 대신 연도 및/또는 국가별로 분리된 예산 데이터가 있다.

데이터세트 시리즈는 [ISO-19115: Geographic information -- Metadata](https://www.iso.org/standard/26020)에서 공통 특성을 공유하는 데이터세트 [...]의 컬렉션으로 정의된다. 그러나 다른 영역에서는 다르게 명명될 수 있고 (예: 시계열, 데이터 슬라이스) 다소 엄격하게 정의될 수 있다(예: [The RDF Data Cube Vocabulary](https://www.w3.org/TR/vocab-data-cube/)에서의 "데이터세트 slice"의 개념).

데이터세트를 시리즈로 그룹화하는 이유와 기준은 여러 가지이며 예를 들어 데이터 특성, 게시 프로세스 및 일반적으로 사용되는 방법 등과 관련될 수 있다. 예를 들어, 지리 공간 데이터와 같이 크기가 큰 데이터는 더 작은 데이터로 분할하면 데이터 공급자와 데이터 소비자가 더 쉽게 처리할 수 있다. 또 다른 예는 연도별로 릴리스되는 데이터로, 일반적으로 시리즈의 첫 번째 데이터에 새 데이터를 추가하는 대신 별도의 데이터 세트를 발행한다.

데이터세트 시리즈를 생성해야 하는 시기와 구성 방법을 결정하기 위한 도메인 전반에 공통 규칙과 기준이 없기 때문에 DCAT에서는 특정 접근 방식을 규정하지 않고 있으며 지침과 도메인 및 커뮤니티 관행을 참고한다. 이 절의 목적은 DCAT에서 데이터세트 시리즈를 지정하는 방법에 대한 지침을 제공하는 것으로 제한한다.

### 데이터세트 지정 방법(How to specify dataset series)

> **NOTE**

> 배포 또는 데이터세트 시리즈의 구성원으로 데이터 세트를 사용하는 것에 대한 논의는 [Issue #868](https://github.com/w3c/dxwg/issues/868)와 [Issue #1429](https://github.com/w3c/dxwg/issues/1429)에서 제기되었다.

DCAT에서 `dcat:Dataset`의 하위 클래스로 정의된 새 클래스 `dcat:DatasetSeries`를 생성하여 데이터세트 시리즈를 데이터 카탈로그의 일급 시민이 되었다. 데이터세트는 `dcat:inSeries` 속성을 사용하여 데이터세트 시리즈에 연결한다. 데이터세트 시리즈는 계층적일 수도 있고 다른 데이터세트 시리즈를 구성할 수도 있다.

다음 예에서 연 예산 데이터를 시리즈로 그룹화한다. 시리즈는 `dcat:DatasetSeries`로 타입을 지정하고 자식 데이터세트는 `dcat:Dataset`로 타입을 지정한다. 데이터세트는 `dcat:inSeries`를 사용하여 시리즈에 연결한다.

> EXAMPLE 37: 시리즈로 그룹핑한 연 예산 데이터세트 <a id="ex-37"></a>

```
ex:EUCatalogue a dcat:Catalog ;
  dcterms:title "European Data Catalogue"@en ;
  dcat:dataset ex:budget , ex:employment , ex:finance ;
  .

ex:budget a dcat:DatasetSeries ;
  dcterms:title "Budget data"@en ;
  .
  
ex:budget-2018 a dcat:Dataset ;
  dcterms:title "Budget data for year 2018"@en ;
  dcat:inSeries ex:budget ;
  .
  
ex:budget-2019 a dcat:Dataset ;
  dcterms:title "Budget data for year 2019"@en ;
  dcat:inSeries ex:budget ;
  .
  
ex:budget-2020 a dcat:Dataset ;
  dcterms:title "Budget data for year 2020"@en ;
  dcat:inSeries ex:budget ;
  .
```

데이터세트 시리즈는 새로운 데이터세트를 수집하여 시간이 지남에 따라 진화할 수 있다. 예를 들어 연 예산 데이터에 대한 데이터세트 시리즈는 매년 새로운 자식 데이터세트를 수집한다. 이러한 경우 연간 릴리스를 첫 번째, 이전, 다음 및 최신 릴리스를 지정하는 관계와 연결하는 것이 중요할 수 있다. 이러한 시나리오에서 DCAT은 각각 `dcat:first`, `dcat:prev` 및 `dcat:last` 속성을 사용한다. `dcat:next`에 대한 [역속성 사용](inverse-property)을 참조하시오.

다음 예는 각 자식 데이터세트의 발행 날짜(`dcterms:issued`)와 시리즈의 이전(`dcat:prev`)과 다음(`dcat:next`) 데이터세트를 지정하여 [EXAMPLE 37](#ex-37)을 확장한다. 또한 데이터세트 시리즈는 첫 번째(`dcat:first`)와 마지막(`dcat:last`) 자식 데이터세트에 연결한다.

> EXAMPLE 38: 시리즈의 데이터세트 연결

```
ex:budget a dcat:DatasetSeries ;
  dcterms:title "Budget data"@en ;
  dcat:first ex:budget-2018 ;
  dcat:last ex:budget-2020 ;
  .
  
ex:budget-2018 a dcat:Dataset ;
  dcterms:title "Budget data for year 2018"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2019-01-01"^^xsd:date ;
  dcat:next ex:budget-2019 ;
  .
  
ex:budget-2019 a dcat:Dataset ;
  dcterms:title "Budget data for year 2019"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2020-01-01"^^xsd:date ;
  dcat:prev ex:budget-2018 ;
  dcat:next ex:budget-2020 ;
  .
  
ex:budget-2020 a dcat:Dataset ;
  dcterms:title "Budget data for year 2020"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2021-01-01"^^xsd:date ;
  dcat:prev ex:budget-2019 ;
  .
```

시리즈의 데이터세트는 물론 버전 관리가 가능하다. 이러한 경우 데이터세트는 [EXAMPLE 39](#ex-39)에서와 같이 [버전 체인과 계층](version-chains-and-hierarchies)에 설명된 접근 방식을 사용하여 해당 버전에 연결할 수 있다.

> **NOTE**

> 이 사용 사례는 Issue [#1409](https://github.com/w3c/dxwg/issues/1409)에 기고되었다.

다음 예는 데이터세트 `ex:budget-2019`에 `ex:budget-2019-rev0`와 `ex:budget-2019-rev1`이라는 두 가지 다른 버전이 있다고 가정하여 [EXAMPLE 38](#ex-38)을 확장한다. 데이터세트 `ex:budget-2019`는 `dcat:hasVersion` 속성을 사용하여 버전에 연결되고 `dcat:hasCurrentVersion` 속성을 사용하여 현재 버전(예:`budget-2019-rev1`)에 연결한다.

> EXAMPLE 39: 데이터세트 시리즈와 버전 결합

```
ex:budget a dcat:DatasetSeries ;
  dcterms:title "Budget data"@en ;
  dcat:first ex:budget-2018 ;
  dcat:last ex:budget-2020 ;
  .
  
ex:budget-2018 a dcat:Dataset ;
  dcterms:title "Budget data for year 2018"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2019-01-01"^^xsd:date ;
  dcat:next ex:budget-2019 ;
  .
  
ex:budget-2019 a dcat:Dataset ;
  dcterms:title "Budget data for year 2019"@en ;
  dcat:inSeries ex:budget ;
  dcat:hasVersion ex:budget-2019-rev0 , ex:budget-2019-rev1 ;
  dcat:hasCurrentVersion ex:budget-2019-rev1 ;
  dcterms:issued "2020-01-01"^^xsd:date ;
  dcat:prev ex:budget-2018 ;
  dcat:next ex:budget-2020 ;
  .

ex:budget-2019-rev0 a dcat:Dataset ;
  dcterms:title "Budget data for year 2019"@en ;
  dcat:version "rev0" ;
  dcat:isVersionOf ex:budget-2019 ;
  dcterms:issued "2020-01-01"^^xsd:date ;
  .

ex:budget-2019-rev1 a dcat:Dataset ;
  dcterms:title "Budget data for year 2019"@en ;
  dcat:version "rev1" ;
  dcat:isVersionOf ex:budget-2019 ;
  dcat:previousVersion ex:budget-2019-rev0 ;
  dcterms:issued "2020-05-10"^^xsd:date ;
  .
  
ex:budget-2020 a dcat:Dataset ;
  dcterms:title "Budget data for year 2020"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2021-01-01"^^xsd:date ;
  dcat:prev ex:budget-2019 ;
  .
```

### 데이터세트 시리즈 메타데이터(Dataset series metadata)
데이터세트 시리즈에 대한 속성은 두 그룹으로 분류할 수 있다.

첫 번째 그룹은 데이터세트 시리즈 자체를 기술하는 속성에 관한 것이다. 예를 들어 `dcterms:accrualPeriodicity` 속성의 경우이다. 이 속성의 값은 새 자식 데이터세트가 추가되는 빈도와 일치해야 한다.

두 번째 그룹은 업스트림 상속을 통해 자식 데이터세트 메타데이터에 기술된 차원을 반영하는 속성에 관한 것이다. 즉, 부모(데이터세트 시리즈)는 자식 데이터세트의 속성 값을 상속한다.

일반적으로 이는 관련 속성 각각에 대해 데이터세트 시리즈가 자식 데이터 세트에 지정된 값의 합집합임을 의미한다. 예를 들어,

- 자식 데이터세트의 시간 적용 범위가 다른 연도(예: 2018, 2019, 2020)인 경우 시리즈의 시간 적용 범위는 2018년에서 2020년 사이의 기간이 된다.
- 자식 데이터세트가 공간 범위로 다른 지리적 경계를 갖는 경우 시리즈의 공간 범위는 이러한 경계(즉, 자식 데이터세트의 경계를 포함하는 경계)의 합집합이 된다.
- 각 자식 데이터세트가 다른 공간 참조 시스템을 사용하는 경우 데이터세트 시리즈에는 여러 공간 참조 시스템이 있다.

마지막으로 자식 데이터세트의 일부 주석 속성은 데이터세트 시리즈 수준에서도 고려해야 할 수 있다. 특히, 자식 데이터세트의 생성/공개/업데이트 날짜와 관련된 속성은 시리즈의 해당 데이터세트에 영향을 미칠 수 있다. 이러한 속성에 대해 DCAT에서는 다음 접근 방식을 권장한다.

- 데이터세트 시리즈의 생성 날짜(`dcterms:created`)는 자식 데이터세트의 가장 이른 생성 날짜와 일치해야 한다.
- 데이터세트 시리즈의 발행일(`dcterms:issued`)은 자식 데이터세트의 가장 이른 발행일과 일치해야 한다.
- 데이터세트 시리즈의 업데이트 날짜(`dcterms:modified`)는 자식 데이터세트의 최신 발행 또는 업데이트 날짜와 일치해야 한다.

> **NOTE**

> 데이터세트 시리즈 메타데이터가 정확하고 업데이트되도록 하기 위해 업스트림 상속을 자동으로 구현하는 메커니즘을 지원할 수 있다. 그러나 DCAT에서는 특정 전략의 채택을 권장하지 않는다.

다음 예는 특정 국가의 연 예산 데이터에 해당하는 자식 데이터세트가 있는 [EXAMPLE 38](#ex-38)의 변형이다. 데이터세트 시리즈의 시간적 해상도(`dcat:temporalResolution`), 시간적 적용 범위(`dcat:temporal`) 및 공간적 적용 범위(`dcat:spatial`)는 자식 데이터세트의 합집합에 해당한다. 또한 데이터세트 시리즈는 첫 번째 발행된 자식 데이터세트 중 하나를 발행 날짜로 지정하고 마지막 자식 데이터세트의 발행 날짜는 업데이트 날짜(`dcterms:modified`)로 지정한다. 마지막으로 자식 데이터세트가 매년 게시되므로 데이터세트 시리즈의 업데이트 빈도(`dcterms:accrualPeriodicity`)는 연이다.

> EXAMPLE 40: 데이터세트 시리즈 메터데이터

```
ex:budget a dcat:DatasetSeries ;
  dcterms:title "Budget data"@en ;
  dcterms:issued "2019-01-01"^^xsd:date ,
  dcterms:modified "2021-01-01"^^xsd:date ;
  dcterms:accrualPeriodicity <http://purl.org/cld/freq/annual> ;
  dcat:temporalResolution "P1Y"^^xsd:duration ;
  dcterms:temporal [ a dcterms:PeriodOfTime ;
    dcat:startDate "2018-01-01"^^xsd:date ;
    dcat:endDate   "2020-12-31"^^xsd:date ;
  ] ;  
  dcterms:spatial <http://publications.europa.eu/resource/dataset/country/BEL> ,
    <http://publications.europa.eu/resource/dataset/country/FRA> ,
    <http://publications.europa.eu/resource/dataset/country/ITA> ,
    ...  ;
  .
  
ex:budget-2018-be a dcat:Dataset ;
  dcterms:title "Belgium budget data for year 2018"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2019-01-01"^^xsd:date ;
  dcat:next ex:budget-2019-be ;
  dcterms:accrualPeriodicity <http://purl.org/cld/freq/annual> ;
  dcat:temporalResolution "P1Y"^^xsd:duration ;
  dcterms:temporal [ a dcterms:PeriodOfTime ;
    dcat:startDate "2018-01-01"^^xsd:date ;
    dcat:endDate   "2018-12-31"^^xsd:date ;
  ] ;  
  dcterms:spatial <http://publications.europa.eu/resource/dataset/country/BEL> ;
  .
  
...
  
ex:budget-2018-fr a dcat:Dataset ;
  dcterms:title "France budget data for year 2018"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2019-01-01"^^xsd:date ;
  dcat:next ex:budget-2019-fr ;
  dcterms:accrualPeriodicity <http://purl.org/cld/freq/annual> ;
  dcat:temporalResolution "P1Y"^^xsd:duration ;
  dcterms:temporal [ a dcterms:PeriodOfTime ;
    dcat:startDate "2018-01-01"^^xsd:date ;
    dcat:endDate   "2018-12-31"^^xsd:date ;
  ] ;  
  dcterms:spatial <http://publications.europa.eu/resource/dataset/country/FRA> ;
  .

...
  
ex:budget-2018-it a dcat:Dataset ;
  dcterms:title "Italy budget data for year 2018"@en ;
  dcat:inSeries ex:budget ;
  dcterms:issued "2019-01-01"^^xsd:date ;
  dcat:next ex:budget-2019-it ;
  dcterms:accrualPeriodicity <http://purl.org/cld/freq/annual> ;
  dcat:temporalResolution "P1Y"^^xsd:duration ;
  dcterms:temporal [ a dcterms:PeriodOfTime ;
    dcat:startDate "2018-01-01"^^xsd:date ;
    dcat:endDate   "2018-12-31"^^xsd:date ;
  ] ;  
  dcterms:spatial <http://publications.europa.eu/resource/dataset/country/ITA> ;
  .
  
...
```

### 기존 DCAT 구현상의 데이터세트 시리즈
기존 DCAT 구현은 데이터세트 시리즈를 지정하기 위해 두 가지 주요 다른 접근 방식을 채택한다.

1. 데이터세트 시리즈는 `dcat:Dataset`으로 타입을 지정하는 반면 자식 데이터 세트는 `dcat:Distribution`으로 타입을 지정한다.
2. 데이터세트 시리즈와 자식 데이터 세트를 모두 `dcat:Dataset` 타입으로 지정하고 일반적으로 둘은 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 속성 `dcterms:hasPart` 또는 `dcterms:isPartOf`를 사용하여 연결한다.

두 경우 모두 데이터세트 시리즈를 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 속성 `dcterms:type`을 사용하여 소프트 유형으로 지정하는 경우가 종종 있다 (예: [GeoDCAT-AP: A geospatial extension for the DCAT application profile for data portals in Europe](https://semiceu.github.io/GeoDCAT-AP/releases/)에서 사용되는 접근 방식이며 [Profilo metadatazione DCAT-AP_IT](https://docs.italia.it/italia/daf/linee-guida-cataloghi-dati-dcat-ap-it/it/stabile/dcat-ap_it.html)과 [GeoDCAT-AP in Italy, the national guidelines published](https://joinup.ec.europa.eu/news/geodcat-apit)에 적용되고 있다).

이 옵션은 공식적으로 DCAT과 호환되므로 DCAT 3으로 업그레이드하는 동안 `dcat:DatasetSeries`와 공존할 수 있다.

## 대이터 인용(Data citation)
*이 절은 공시 표준에 포함된 것이 아니다.*

데이터세트 인용은 이 DCAT 개정판의 새로운 요구 사항 중 하나이다. 데이터 인용은 서지 참조를 제공할 때와 유사한 방식으로 데이터를 참조하는 관행으로, 모든 조사 과정에서 데이터를 일급 출력으로 인정한다. 데이터 인용은 데이터 생성자에 대한 적절한 귀속과 신용 지원, 데이터 검색 촉진, 데이터 영향과 재사용 추적 지원, 데이터의 협업 및 재사용 허용, 데이터를 기반으로 한 결과의 재현성 가능 등과 같은 여러 이점을 제공한다.

데이터 인용을 지원하기 위해 데이터세트 기술에는 최소한 데이터세트 식별자, 데이터세트 작성자, 데이터세트 제목, 데이터세트 게시자 및 데이터세트 발행 또는 릴리스 날짜가 포함되어야 한다. 이러한 요소는 DataCite 메타데이터 스키마([DataCite Metadata Schema](https://schema.datacite.org/))가 필요한 요소이며, 이는 [DataCite Metadata Schema](https://schema.datacite.org/)가 데이터를 조사하기 위해 할당한 영구 식별자(디지털 객체 식별자 또는 DOI)와 연결된 메타데이터이다.

데이터 인용을 지원하기 위해 이 DCAT 개정판은 ㅔ역참조 가능한 식별자]()에 대한 고려와 [목록화된 자원의 작성자]()를 표시하는 지원을 추가했다. 데이터 인용에 필요한 나머지 속성을 DCAT 1([Data Catalog Vocabulary (DCAT)](https://www.w3.org/TR/vocab-dcat-1/))에서 이미 사용할 수 있다.

데이터세트 설명에서 데이터 인용에 필요한 속성의 가용성에 대한 제약은 DCAT 데이터 인용 프로필로 나타낼 수 있다.

## 품질 정보(Quality information)
*이 절은 공시 표준에 포함된 것이 아니다.*

> **NOTE**

> 이 절은 DCAT 일급 개체(예: 데이터세트, 배포)의 품질을 문서화하는 방법에 대한 지침을 제공하고 새로운 DCAT 용어를 정의하지 않기 때문에 표준이 아니다. 이 지침은 W3C 그룹 노트인 데이터 품질 어휘(DQV)([Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/))에 의존한다.

데이터 품질 어휘(DQV)([Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/))는 데이터 품질의 다양한 측면에 대한 공통 모델링 패턴을 제공한다. DCAT 데이터세트와 배포는 다음을 포함하는 품질 정보의 다양한 타입과 관계가 있다.

- [`dqv:QualityAnnotation`](https://www.w3.org/TR/vocab-dqv/#dqv:QualityAnnotation)은 데이터세트 또는 해당 배포에 대해 제공된 피드백과 품질 인증서를 나타낸다.
- [`dqv:QualityPolicy`](https://www.w3.org/TR/vocab-dqv/#dqv:QualityPolicy)는 주로 데이터 품질 관련 문제를 관리하는 정책 또는 계약을 나타낸다.
- [`dqv:QualityMeasurement`](https://www.w3.org/TR/vocab-dqv/#dqv:QualityMeasurement)는 데이터세트 또는 배포에 대한 정량적 또는 정성적 정보를 제공하는 메트릭 값을 나타낸다.

품질 정보의 각 타입은 하나 이상의 품질 차원, 즉 소비자와 관련된 품질 특성과 관련될 수 있다. 품질을 다차원 공간으로 보는 관행은 품질 관리를 처리 가능한 청크로 분할하기 위해 품질 관리 분야에서 통합된다. DQV는 품질 차원의 표준 목록을 정의하지 않는다. ISO/IEC 25012 ([ISO/IEC 25012 - Data Quality model](http://iso25000.com/index.php/en/iso-25000-standards/iso-25012))와 [Quality assessment for Linked Data: A Survey](https://doi.org/10.3233/SW-150175)에서 제안된 품질 차원을 두 가지 가능한 출발점으로 제공한다. 또한 후자에 정의된 품질 차원과 범주에 대한 [RDF 표현](https://www.w3.org/2016/05/ldqd)을 제공한다. 궁극적으로 구현자는 자신의 요구에 가장 잘 맞는 품질 차원의 컬렉션을 스스로 선택해야 한다. 다음 항에서 DCAT와 DQV를 결합하여 데이터세트와 배포의 품질을 기술하는 방법을 설명한다. 포괄적인 소개 및 추가적인 사용 예는 [Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/)를 참조하시오.

> **NOTE**

> 다음 예에서는 품질 정보가 있는 위치와 관리 방법에 대해 언급하지 않는다. 이는 DCAT 어휘의 범위를 벗어나기 때문이다. 표시된 IRI를 사용하여 우수한 개인이 있다고 가정한다. 게다가 예와 [Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/)의 보다 일반적으로 품질 정보를 수집하는 방법에 대한 데이터 포털 디자인 선택은 중립적이다. 예를 들어 데이터 포털은 특정 UI를 구현하여 데이터에 주석을 달거나 서드파티 서비스에서 입력을 받아 [Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/) 인스턴스를 수집할 수 있다.

### 품질 정보 제공(Providing quality information)
데이터 소비자(`ex:consumer1`)는 제노바의 버스 정류장의 지리 참조 목록을 포함하는 데이터세트 `ex:genoaBusStopsDataset`의 품질을 기술한다. 그/그녀는 데이터 완전성(`ldqd:completeness`)에 대한 DQV 품질 메모(`ex:genoaBusStopsDatasetCompletenessNote`)로 데이터세트에 주석을 달아 데이터세트에 30000개의 정류장 중 20500개만 포함되어 있다고 주석을 달았다.

> EXAMPLE 41

```
ex:genoaBusStopsDataset a dcat:Dataset ;
  dqv:hasQualityAnnotation ex:genoaBusStopsDatasetCompletenessNote .

ex:genoaBusStopsDatasetCompletenessNote a dqv:UserQualityFeedback ;
  oa:hasTarget ex:genoaBusStopsDataset ;
  oa:hasBody ex:textBody ;
  oa:motivatedBy dqv:qualityAssessment ;
  prov:wasAttributedTo ex:consumer1 ;
  prov:generatedAtTime "2018-05-27T02:52:02Z"^^xsd:dateTime ;
  dqv:inDimension ldqd:completeness
  .

ex:textBody a oa:TextualBody ;
  rdf:value "Incomplete dataset: it contains only 20500 out of 30000 existing bus stops" ;
  dc:language "en" ;
  dc:format "text/plain"
  .
```

`ex:myQualityChecking` 활동은 `ex:myQualityChecker` 서비스를 사용하여 `ex:genoaBusStopsDataset` 데이터세트의 품질을 확인한다. `ex:completenessWRTExpectedNumberOfEntities` 메트릭은 데이터세트 완전성(`ldqd:completeness`)을 측정하는 데 적용되며 결과적으로 품질 측정(`ex:genoaBusStopsDatasetCompletenessMeasurement`)이 된다.

> EXAMPLE 42

```
ex:genoaBusStopsDataset
  dqv:hasQualityMeasurement ex:genoaBusStopsDatasetCompletenessMeasurement .

ex:genoaBusStopsDatasetCompletenessMeasurement a dqv:QualityMeasurement ;
  dqv:computedOn ex:genoaBusStopsDataset ;
  dqv:isMeasurementOf ex:completenessWRTExpectedNumberOfEntities ;
  dqv:value "0.6833333"^^xsd:decimal  ;
  prov:wasAttributedTo ex:myQualityChecker ;
  prov:generatedAtTime "2018-05-27T02:52:02Z"^^xsd:dateTime ;
  prov:wasGeneratedBy ex:myQualityChecking
  .

ex:completenessWRTExpectedNumberOfEntities a dqv:Metric ;
  skos:definition "The degree of completeness as ratio between the actual number of entities included in the dataset and the declared expected number of entities."@en ;
  dqv:expectedDataType xsd:decimal ;
  dqv:inDimension ldqd:completeness .

# ex:myQualityChecker is a service computing some quality metrics
ex:myQualityChecker a prov:SoftwareAgent ;
  rdfs:label "A quality assessment service"@en .
  # Further details about quality service/software can be provided, for example,
  # deploying  vocabularies such as Dataset Usage Vocabulary (DUV), Dublin Core or ADMS.SW

# ex:myQualityChecking is the activity that has generated 
# ex:genoaBusStopsDatasetCompletenessMeasurement from ex:genoaBusStopsDataset
ex:myQualityChecking a prov:Activity ;
  rdfs:label "The checking of genoaBusStopsDataset's quality"@en ;
  prov:wasAssociatedWith ex:myQualityChecker ;
  prov:used ex:genoaBusStopsDataset ;
  prov:generated ex:genoaBusStopsDatasetCompletenessMeasurement ;
  prov:endedAtTime "2018-05-27T02:52:02Z"^^xsd:dateTime ;
  prov:startedAtTime "2018-05-27T00:52:02Z"^^xsd:dateTime .
```

[데이터세트 정확도와 정밀도를 표현하는 방법](https://www.w3.org/TR/vocab-dqv/#ExpressDatasetAccuracyPrecision)에 대한 예를 포함하여 [Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/)에서 품질 문서의 다른 예를 볼 수 있다.

### 표준 준수 문서화(Documenting conformance to standards)
이 항은 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)와 EARL([Evaluation and Report Language (EARL) 1.0 Schema](https://www.w3.org/TR/EARL10-Schema/))를 [Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/)와 결합하여 명시된 품질 표준에 대한 적합성 정도와 적합성 테스트에 대한 세부 정보를 나타내는 다양한 모델링 패턴을 보여준다.

#### 표준 준수(Conformance to a standard)
[`dcterms:conformsTo`](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/conformsTo)와 [`dcterms:Standard`](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/Standard)의 사용은 표준 준수를 나타내는 잘 알려진 패턴입니다. [Spatial Data on the Web Best Practices](https://www.w3.org/TR/sdw-bp/)([EXAMPLE 51](https://www.w3.org/TR/sdw-bp/#ex-geodcat-ap-dataset-conformance-with-specification))에서 직접 차용한 [EXAMPLE 43](#ex-43)은 가상의 `dcat:Dataset`이 공간 데이터세트와 서비스의 상호 운용성에 관한 EU INSPIRE 규정을 준수함을 선언한다 (["Commission Regulation (EU) No 1089/2010 of 23 November 2010 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards interoperability of spatial data sets and services"](http://data.europa.eu/eli/reg/2014/1312/oj)).

> EXAMPLE 43 <a id="ex-43"></a>

```
ex:Dataset-1 a dcat:Dataset;
  dcterms:conformsTo <http://data.europa.eu/eli/reg/2014/1312/oj> .

# Reference standard / specification
<http://data.europa.eu/eli/reg/2014/1312/oj> a dcterms:Standard ;
  dcterms:title "Commission Regulation (EU) No 1089/2010 of 23 November 2010 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards interoperability of spatial data sets and services"@en ;
  dcterms:issued "2010-11-23"^^xsd:date .
```

또 다른 예는 일반적으로 지리 공간 메타데이터에 포함되는 정보인 데이터세트에 사용되는 좌표 참조 시스템(CRS)의 사양에 관한 것이다. [EXAMPLE 44](#ex-44)는 DCAT에서 데이터세트의 CRS를 지정하는 방법을 보인다.

> EXAMPLE 44 <a id="ex-44"></a>

```
ex:Dataset-2 a dcat:Dataset;
  dcterms:conformsTo <http://www.opengis.net/def/crs/EPSG/0/28992> .
```

[EXAMPLE 44](#ex-44)에서 `http://www.opengis.net/def/crs/EPSG/0/28992`는 [EPSG:28992](http://www.epsg-registry.org/?display=entity&urn=urn:ogc:def:crs:EPSG::28992)("Amersfoort / RD New")에 해당하는 OGC CRS 레지스트리의 IRI이다([EXAMPLE 30](#ex-30) 참조). 

> **NOTE**

> 리소스 CRS의 제공은 [Spatial Data on the Web Best Practices](https://www.w3.org/TR/sdw-bp/)의 모범 사례 8(좌표 값을 인코딩하는 방법 서술)을 준수한다.

상호 운용성을 보장하기 위해 참조 표준/사양을 식별하는 IRI를 일관되게 사용하는 것이 중요하다. 특히 DCAT에서는 다음과 같은 일반 규칙을 권장하고 있다.

- 사용 가능한 경우 참조 레지스트리의 IRI를 사용한다. 예로는 [W3C TR 레지스트리](), [OGC 정의 서버]()와 [ISO OBP]()가 있다.
- 네임스페이스 IRI가 아닌 표준/사양의 IRI를 사용한다. 예를 들어, DCAT과 `dcat:CatalogRecord`의 일치를 표현하기 위해 사용되는 IRI는 `http://www.w3.org/ns/dcat#`이 아니라 `https://www.w3.org/TR/vocab-dcat/`이다. .
- 정식 영구 IRI를 사용한다. 이는 일반적으로 문서 자체에 지정된다. 확실하지 않은 경우 해당 표준/사양에 대한 서지 인용에 포함된 것을 사용한다.
- 버전이 지정되지 않은 IRI를 사용한다. 특정 버전의 표준/사양에 대한 적합성을 표현해야 하는 경우 버전 없는 IRI와 버전이 있는 IRI를 모두 사용한다. 예를 들어 `dcat:CatalogRecord`의 DCAT 2 적합성을 명시적으로 기술해야 하는 경우 `https://www.w3.org/TR/vocab-dcat/`와 `https://www.w3.org/TR/vocab-dcat-2/`을 모두 사용한다.

[RXAMPLE 45](#ex-45)는 위의 규칙에 따라 주어진 카탈로그 레코드가 DCAT를 준수하도록 지정하는 방법을 보여주기 위해 [EXAMPLE 9](#ex-9)를 확장하였다.

> EXAMPLE 45 <a id="ex-45"></a>

```
ex:catalog dcat:record ex:record-001 .

ex:record-001
  a dcat:CatalogRecord ;
  foaf:primaryTopic ex:dataset-001 ;
  dcterms:issued "2011-12-11"^^xsd:date ;
  dcterms:conformsTo <https://www.w3.org/TR/vocab-dcat/> ;
  .
```

아래 표는 이 문서에 포함된 예들에서 사용된 일부 표준 IRI들이다.

> EXAMPLE 46: 표준 IRI

| IRI | Specification |
|-----|---------------|
| `https://www.w3.org/TR/owl2-overview/` | OWL 2 Web Ontology Language Document Overview (Second Edition) |
| `https://www.w3.org/TR/rdf-schema/` | RDF Schema 1.1 |
| `https://www.w3.org/TR/sparql11-protocol/` | SPARQL 1.1 Protocol |
| `http://www.opengis.net/def/serviceType/ogc/csw/` | Catalogue Services 3.0 - General Model |
| `http://www.opengis.net/def/serviceType/ogc/wfs/` | Web Feature Service 2.0 Interface Standard |
| `http://www.opengis.net/def/serviceType/ogc/wms/` | Web Map Service Implementation Specification |

#### 적합성 정도(Degree of conformance)
일부 법적 맥락에서는 적합성 정도를 지정해야 한다. 예를 들어, INSPIRE 메타데이터는 완전한 준수 외에 부적합 및 비평가를 표현하기 위해 특정 제어 어휘[INSPIRE Registry: Degrees of conformity](http://inspire.ec.europa.eu/metadata-codelist/DegreeOfConformity/)를 채택한다. 유사한 통제 어휘를 다른 맥락에서 정의할 수 있다.

[EXAMPLE 47](#ex-47)은 적합성(즉, 적합성, 비준수성)의 정도를 나타내는 몇 가지 새로 생성된 개념을 지정하고 적합성 테스트 결과를 나타내기 위해 [`dcterms:type`](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/type)을 선언한다. [GeoDCAT-AP: A geospatial extension for the DCAT application profile for data portals in Europe](https://semiceu.github.io/GeoDCAT-AP/releases/)에 사용된 패턴에 따라 이 예에서는 `prov:Entity`를 사용하여 적합성 테스트(예: `ex:testResult`)를 모델링하고, `prov:Activity`를 사용하여 테스트 활동(예: `ex:testingActivity`)을 모델링한다. `prov:Plan` [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)(예: `ex:conformanceTest`)에서 파생된 계획을 통해 전체 모범 사례 집합을 확인한다. 자격을 갖춘 PROV 연결은 테스트 활동을 적합성 테스트에 바인딩한다.

> **NOTE**

> 데이터 세트의 종류에 따라 FAIR 원칙([The FAIR Guiding Principles for scientific data management and stewardship](https://doi.org/10.1038/sdata.2016.18)) 또는 공간 데이터의 웹 모범 사례[Spatial Data on the Web Best Practices](https://www.w3.org/TR/sdw-bp/)와 같은 다른 모범 사례와 표준을 대체하거나 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)와 함께 사용할 수 있다. 

> EXAMPLE 47

```
ex:Dataset-3 a dcat:Dataset ;
  prov:wasUsedBy ex:testingActivity .

ex:testingActivity a prov:Activity ;
  prov:generated ex:testResult ;
  prov:qualifiedAssociation [
    a prov:Association ;
    # http://validator.example.org/ is the agent who ran the test.
    prov:agent  <http://validator.example.org/>
    # following the plan ex:conformanceTest
    prov:hadPlan ex:conformanceTest
  ] .

# Conformance test result
ex:testResult a prov:Entity ;
  prov:wasGeneratedBy ex:testingActivity;
  # ex:notConformant belongs to a SKOS concept scheme about conformance
  dcterms:type ex:notConformant .

ex:conformanceTest a prov:Plan ;
  # Here one can specify additional information on the test, which in the example is derived by the W3C Data on the Web Best Practices
  prov:wasDerivedFrom <https://www.w3.org/TR/dwbp/> .
```

또한 [Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/)를 배포하여 특정 표준에 대한 준수 여부를 측정할 수 있다. [EXAMPLE 48]()#ex-48)에서 `ex:levelOfComplianceToDWBP`는 통과된 준수 테스트의 백분율로 [Data on the Web Best Practices](https://www.w3.org/TR/dwbp/)에 대한 데이터세트의 준수를 측정하는 품질 메트릭이다. [EXAMPLE 48]()#ex-48)은 [ISO/IEC 25012 - Data Quality model](http://iso25000.com/index.php/en/iso-25000-standards/iso-25012)에 정의된 품질 차원과 범주를 나타내는 네임스페이스 접두사로 `iso`를 가정한다.

> EXAMPLE 48 <a id="ex-48"></a>

```
ex:levelOfComplianceToDWBP a dqv:Metric ;
  skos:definition "The degree of compliance to DWBP defined as the percentage of passed compliance tests."@en ;
  dqv:expectedDataType xsd:double ;
  dqv:inDimension  iso:compliance .

iso:compliance a dqv:Dimension ;
  skos:prefLabel "Compliance"@en ;
  skos:definition "The degree to which data has attributes that adhere to standards, conventions or regulations in force and similar rules relating to data quality in a specific context of use."@en ;
  dqv:inCategory iso:inherentAndSystemDependentDataQuality .
  
iso:inherentAndSystemDependentDataQuality a dqv:Category ;
  skos:prefLabel "Inherent and System-Dependent Data Quality"@en 
```

품질 측정 `ex:measurement_complianceToDWBP`는 데이터세트 `ex:Dataset`에 대한 준수 수준, 즉 `ex:levelOfComplianceToDWBP` 메트릭의 측정을 나타낸다. 적합성 테스트의 일부만 성공하면(예: 적합성 테스트의 절반) 측정은 [EXAMPLE 49](#ex-49)와 같다.

> EXAMPLE 49

```
ex:measurement_complianceToDWBP a dqv:QualityMeasurement ;
  dqv:computedOn ex:Dataset ;
  dqv:value "50"^^xsd:double ;
  sdmx-attribute:unitMeasure <http://www.wurvoc.org/vocabularies/om-1.8/Percentage> ;
  dcterms:date "2018-01-10"^^xsd:date ;
  dqv:isMeasurementOf ex:levelOfComplianceToDWBP .
```

#### 적합성 시험 결과(Conformance test results)
테스트에 대한 추가 정보를 EARL([Evaluation and Report Language (EARL) 1.0 Schema](https://www.w3.org/TR/EARL10-Schema/))를 사용하여 제공할 수 있다. EARL은 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)와 함께 채택할 수 있는 테스트 활동을 설명하는 특정 클래스를 제공한다. [EXAMPLE 50](#ex-50)은 `prov:Activity`에 대한 정규화된 연결 대신 `earl:Assertion`으로 테스트 활동 `ex:testingActivity`를 기술한다. `earl:Assertion`은 데이터세트 `ex:Dataset`이 `ex:conformanceTest` 적합성 테스트로 테스트되었으며 `ex:testResult`에 설명된 대로 테스트를 통과했다고 명시한다.

> EXAMPLE 50 <a id="ex-50"></a>

```
ex:assertion a earl:Assertion ;
  earl:subject ex:Dataset ;
  earl:test ex:conformanceTest ;
  earl:result ex:testResult ;
  # let's indicate if the test was manual, automatic, or what ..
  earl:mode earl:automatic ;
  earl:assertedBy <http://validator.example.org/> ;
  prov:wasAttributedTo  <http://validator.example.org/> .

ex:conformanceTest a earl:TestRequirement, prov:Plan ;
  dcterms:title "Set of conformance test derived from the W3C Data on the Web Best Practices"@en ;
  # it includes different subtests
  dcterms:hasPart ex:testBP1, ex:testBP2, ...,  ex:testBP35 ;
  #It is derived  by the reference standard
  prov:wasDerivedFrom  <https://www.w3.org/TR/dwbp/> .

ex:testResult a earl:TestResult ;
  #  results in conformancy .
  dcterms:type  ex:conformant ;
  prov:wasGeneratedBy ex:testingActivity;
  #the overall set of tests have been passed
  earl:outcome earl:passed .

# the description of the validator
<http://validator.example.org/> a earl:Assertor, prov:Agent ;
  dcterms:description "A test execution service that runs conformance test suites."@en ;
  dcterms:title "Validator"@en .

#the testing activity
ex:testingActivity a prov:Activity ;
  prov:generated ex:assertion, ex:testResult ;
  prov:use ex:Dataset ;
  prov:wasAssociatedWith <http://validator.example.org/> .
```

[EXAMPLE 51](#ex-51)은 하위 테스트 `ex:testq1`이 실패한 경우 기술 방법을 보이고 있다. 특히 `dcterms:description`과 `earl:info`는 사람이 읽을 수 있는 형식으로 추가 경고 또는 오류 메시지를 제공한다.

> EXAMPLE 51

```
ex:assertion1 a earl:Assertion ;
  earl:subject ex:Dataset ;
  earl:test ex:testq1 ;
  earl:result [
    a earl:TestResult ;
    #  results in no conformancy
    dcterms:type ex:nonConformant ;
    #the overall set of tests have not been passed (!?)
    dcterms:date "2015-09-29T11:50:00+00:00"^^xsd:dateTime ;
    # Some XML encoding of the error
      dcterms:description """
        <ul xmlns="http://www.w3.org/1999/xhtml">
          <li> test 1 has failed. Some description of the errors found</li>
        </ul>"""^^rdf:HTML ;
      earl:info """
        <test-method duration-ms="47" finished-at="2015-09-29T11:50:00Z"
        name="validate" signature="validate()" started-at="2015-09-29T11:50:00Z"
        status="FAIL">
          <exception class="java.lang.AssertionError">
            <message>
              Total validation errors found: 2
            </message>
          </exception>
        </test-method>"""^^rdf:XMLLiteral ;
     earl:outcome earl:fail .
  ];
  # we do not know if the test was manual, automatic, or what ..
  earl:mode earl:automatic.
```

테스트에 필요한 세부 사항에 따라 [Data on the Web Best Practices: Data Quality Vocabulary](https://www.w3.org/TR/vocab-dqv/) 테스트 활동과 오류도 표현할 수 있다. [EXAMPLE 52](#ex-52)에서 `ex:error`는 이전 오류를 나타내는 품질 주석이고, `ex:testResult`를 위의 주석과 출처 정보를 제공하는 준수 측정을 수집하기 위한 `dqv:QualityMetadata`로 정의한다.

> EXAMPLE 52

```
ex:errors
  a dqv:QualityAnnotation ;
  #this annotation is derived by the measurement
  prov:wasGeneratedBy  ex:testingActivity;
  oa:hasTarget ex:Dataset ;
  oa:hasBody [
    #errors/failed test description
    a oa:TextualBody ;
    rdf:value  """
      <test-method duration-ms="47" finished-at="2015-09-29T11:50:00Z"
        name="validate" signature="validate()" started-at="2015-09-29T11:50:00Z"
        status="FAIL">
        <exception class="java.lang.AssertionError">
          <message>
            Total validation errors found: 2
          </message>
        </exception>
      </test-method>"""^^rdf:XMLLiteral ;
    #it can be in any format supported by dc
    dc:format  "application/xml" ;
  ] ;
  oa:motivatedBy dqv:qualityAssessment , oa:assessing ;
  dqv:inDimension iso:compliance ;
  .

ex:testResult
  a dqv:QualityMetadata ;
  #  change the the dcterms:type according to the resulted compliance
  dcterms:type <http://inspire.ec.europa.eu/metadata-codelist/DegreeOfConformity/conformant> ;
  prov:wasAttributedTo <http://validator.example.org/> ;
  prov:generatedAtTime "2018-05-27T02:52:02Z"^^xsd:dateTime ;
  prov:wasGeneratedBy ex:testingActivity .

# The graph contains the rest of the statements presented in the previous examples.
# The graph is expressed according to TRIG syntax not TTL (see https://www.w3.org/TR/trig/)
ex:testResult {
  a dcat:Dataset ;
  dqv:hasQualityMeasurement ex:measurement_complianceToDWBP ;
  dqv:hasQualityAnnotation ex:errors .
}

#the testing activity
ex:testingActivity a prov:Activity ;
  prov:generated  ex:testResult ;
  prov:use ex:Dataset ;
  prov:wasAssociatedWith <http://validator.example.org/> ;
  .
```

물론 위의 모델링 패턴은 표준 준수뿐만 아니라 모든 품질 테스트를 나타낼 수 있다.

## 한정된 관계(Qualified relations) <a id="qualified-relations"></a>
*이 절은 공시 표준에 포함된 것이 아니다.*

DCAT에는 데이터세트와 데이터 서비스의 여러 측면에 대한 기술을 지원하는 요소가 포함되어 있다. 그럼에도 불구하고 일부 관계의 의미를 완전히 표현하려면 추가 정보가 필요하다. 예를 들어 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)는 리소스를 책임 당사자 또는 에이전트에 귀속시키기 위한 표준 역할로 **creator**, **contributor**와 **publisher**를 제공하지만 다른 많은 잠재적 역할이 있다. 예를 들어 [Geographic information -- Metadata -- Part 1: Fundamentals](https://www.iso.org/standard/53798.html)의 [CI_RoleCode](https://standards.iso.org/iso/19115/resources/Codelists/gml/CI_RoleCode.xml) 값을 참조하시오. 유사하게 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)과 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)는 **was derived from**, **was quoted from**, **is version of**, **references** 및 기타 여러 관계를 포함하여 리소스 간의 관계를 캡처하는 몇 가지 속성을 제공하지만, [Geographic information -- Metadata -- Part 1: Fundamentals](https://www.iso.org/standard/53798.html) [DS_AssociationTypeCodes](https://standards.iso.org/iso/19115/resources/Codelists/gml/DS_AssociationTypeCode.xml)의 목록, 링크 관계의 IANA 레지스트리([Link Relations](https://www.iana.org/assignments/link-relations/)), DataCite 메타데이터 스키마 ([DataCite Metadata Schema](https://schema.datacite.org/)) 및 [MARC relators](https://id.loc.gov/vocabulary/relators)에 추가적으로 관련이 있다. 이러한 관계는 `dcterms:relation`, `dcterms:contributor` 등의 추가 하위 속성으로 캡처할 수 있지만, 이는 속성 수의 폭발로 이어질 것이며 어쨌든 잠재적 역할과 관계의 전체 집합을 알 수 없다. 

이러한 종류의 요구 사항을 충족하기 위한 일반적인 접근 방식은 관계를 한정하는 매개변수를 전달하는 추가 리소스를 도입하는 것이다. 선례는 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)의 [한정된 용어](https://www.w3.org/TR/prov-o/#description-qualified-terms)와 Semantic Sensor Network 온톨로지 ([Semantic Sensor Network Ontology](https://www.w3.org/TR/vocab-ssn/))의 [샘플 관계](https://www.w3.org/TR/vocab-ssn/#Sample_Relations)이다. 일반적인 [Qualified Relation pattern](http://patterns.dataincubator.org/book/qualified-relation.html)은 [Linked Data Patterns: A pattern catalogue for modelling, publishing, and consuming Linked Data](http://patterns.dataincubator.org/book/)에 기술되어 있다.

[PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)의 많은 한정된 용어는 카탈로그의 자원 기술과 관련이 있지만 PROV-O가 취하는 활동 중심 관점으로 인해 불완전하다. 일부 격차를 해결하기 위해 DCAT 어휘를 추가 형식으로 포함하여 명시적 활동에 포함되지 않는 요구 사항을 충족시킨다. 이는 [Fig. 6](#fig-6)에 요약되어 있다.

![Fig. 6](images/dcat-relationships.svg)
<center>Fig. 6 한정된 관계는 에이전트 또는 기타 리소스에 리소스를 관련시키는 확장 가능한 역할들을 지원한다.</center> <a id="fig-6"></a>

이러한 한정된 형식의 초점은 관계에 대한 추가 역할을 허용하는 것이지만 적용 가능한 시간 간격과 같은 관계의 다른 측면은 특정 노드가 이와 같은 관계를 기술하는 데 사용될 때 쉽게 연결될 수 있다 (예: 몇 가지 예를 보려면 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)의 영향 관계 차트를 참조하시오).

> **NOTE**

> [`prov:qualifiedAttribution`](https://www.w3.org/TR/prov-o/#qualifiedAttribution)과 [`dcat:qualifiedRelation`](https://w3c.github.io/dxwg/dcat/#Property:resource_qualified_relation)의 상위 속성에 대한 전역 도메인 제약으로 인해 한정된 형식을 사용하려면 컨텍스트 리소스가 [`prov:Entity`](https://www.w3.org/TR/prov-o/#Entity) 클래스의 구성원이어야 한다 ([RDF Schema 1.1](https://www.w3.org/TR/rdf-schema/)).

### 데이터세트와 에이전트간의 관계(Relationships between datasets and agents)
표준 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 속성 [`dcterms:contributor`](http://purl.org/dc/terms/contributor), [`dcterms:creator`](http://purl.org/dc/terms/creator) 및 [`dcterms:publisher`](http://purl.org/dc/terms/publisher)와 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)의 일반 [`prov:wasAttributedTo`](https://www.w3.org/TR/prov-o/#wasAttributedTo)는 카탈로그된 리소스와 책임 에이전트의 기본 연결을 지원한다. 그러나 데이터세트와 서비스와 관련하여 자금 제공자(funder), 배포자(distributor), 관리인(custodian), 편집자(editor) 등과 같은 다른 많은 중요한 역할이 있다. 이러한 역할 중 일부는 [Geographic information -- Metadata -- Part 1: Fundamentals](https://www.iso.org/standard/53798.html), [DataCite Metadata Schema](https://schema.datacite.org/) 메타데이터 스키마의 [CI_RoleCode](https://standards.iso.org/iso/19115/resources/Codelists/gml/CI_RoleCode.xml) 값에 열거되고 [MARC 관련자](https://id.loc.gov/vocabulary/relators) 내에 포함되어 있다.

지정된 역할을 가진 자원에 에이전트를 할당하는 일반적인 방법은 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)의 정규화된 형식 [`prov:qualifiedAttribution을`](https://www.w3.org/TR/prov-o/#qualifiedAttribution) 사용하여 제공된다. [EXAMPLE 53](#ex-53)은 그 예를 보인다.

> EXAMPLE 53 <a id="ex-53"></a>

```
ex:DS987
  a dcat:Dataset ;
  prov:qualifiedAttribution [
    a prov:Attribution ;
    prov:agent <https://www.ala.org.au/> ;
    dcat:hadRole <http://registry.it.csiro.au/def/isotc211/CI_RoleCode/distributor>
  ] ;
  prov:qualifiedAttribution [
    a prov:Attribution ;
    prov:agent <https://www.education.gov.au/> ;
    dcat:hadRole <http://registry.it.csiro.au/def/isotc211/CI_RoleCode/funder>
  ] ;
.
```

[EXAMPLE 53](#ex-53)에서 역할은 [Geographic information -- Metadata -- Part 1: Fundamentals](https://www.iso.org/standard/53798.html)의 [CI_RoleCode](https://standards.iso.org/iso/19115/resources/Codelists/gml/CI_RoleCode.xml) 코드 목록의 (비표준) [연결된 데이터 표현](http://registry.it.csiro.au/def/isotc211/CI_RoleCode)의 IRI로 표시되었다.

> **NOTE**

> [`prov:hadRole`](https://www.w3.org/TR/prov-o/#hadRole) 속성의 도메인은 [`prov:Association`](https://www.w3.org/TR/prov-o/#Association)이다. 즉, [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/) 역할은 엔티티 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)가 아니라 활동과 관련된다. 따라서 새 속성 [`dcat:hadRole`](https://w3c.github.io/dxwg/dcat/#Property:relationship_hadRole)을 사용하여 연관 클래스 [`prov:Attribution`](https://www.w3.org/TR/prov-o/#Attribution)에 특정 역할을 연결한다.

### 데이터세트와 다은 리소스간의 관계(Relationships between datasets and other resources)
표준 [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 속성 [`dcterms:relation`](http://purl.org/dc/terms/relation)과 [`dcterms:hasPart`](http://purl.org/dc/terms/hasPart) / [`dcterms:isPartOf`](http://purl.org/dc/terms/isPartOf), [`dcterms:hasVersion`](http://purl.org/dc/terms/hasVersion) / [`dcterms:isVersionOf`](http://purl.org/dc/terms/isVersionOf), [`dcterms:replaces`](http://purl.org/dc/terms/replaces) / [`dcterms:isReplacedBy`](http://purl.org/dc/terms/isReplacedBy), [`dcterms:requires`](http://purl.org/dc/terms/requires) / [`dcterms:isRequiredBy`](http://purl.org/dc/terms/isRequiredBy), [`prov:wasDerivedFrom`](http://www.w3.org/TR/prov-o/#wasDerivedFrom), [`prov:wasQuotedFrom`](http://www.w3.org/TR/prov-o/#wasQuotedFrom) 등과 같은 하위 속성 데이터세트와 다른 카탈로그된 리소스 간의 관계에 대한 기술을 지원한다. 그러나 다른 중요한 관계가 많이 있다. 예를 들어 alternate, canonical, original, preview, stereo-mate, working-copy-of 등이 있다. 이러한 역할 중 일부는 [Geographic information -- Metadata -- Part 1: Fundamentals](https://www.iso.org/standard/53798.html)의 [DS_AssociationTypeCodes](https://standards.iso.org/iso/19115/resources/Codelists/gml/DS_AssociationTypeCode.xml) 값, 링크 관계의 IANA 레지스트리 [Link Relations](https://www.iana.org/assignments/link-relations/), [DataCite Metadata Schema](https://schema.datacite.org/) 메타데이터 스키마에 열거되고 [MARC 관련자](https://id.loc.gov/vocabulary/relators) 내에 포함된다.

리소스를 지정된 역할을 가진 다른 리소스와 연관시키는 일반적인 방법은 [`dcat:qualifiedRelation`](https://w3c.github.io/dxwg/dcat/#Property:resource_qualified_relation)에 규정된 형식을 사용하여 제공된다. [EXAMPLE 54](#ex-54)은 그 예를 보인다.

> EXAMPLE 54 <a id="ex-54"></a>

```
ex:Test987
  a dcat:Dataset ;
  dcat:qualifiedRelation [
    a dcat:Relationship ;
    dcterms:relation <http://dcat.example.org/Original987> ;
    dcat:hadRole <http://www.iana.org/assignments/relation/original>
  ] ;
.

ex:Test543L
  a dcat:Dataset ;
  dcat:qualifiedRelation [
    a dcat:Relationship ;
    dcterms:relation <http://dcat.example.org/Test543R> ;
    dcat:hadRole <http://registry.it.csiro.au/def/isotc211/DS_AssociationTypeCode/stereoMate>
  ] ;
.
```

[EXAMPLE 54](#ex-54)에서 역할은 [Link Relations](https://www.iana.org/assignments/link-relations/)의 IRI와 [Geographic information -- Metadata -- Part 1: Fundamentals](https://www.iso.org/standard/53798.html)의 [DS_AssociationTypeCode](https://standards.iso.org/iso/19115/resources/Codelists/gml/DS_AssociationTypeCode.xml) 코드 목록의 (비표준) [연결된 데이터 표현](http://registry.it.csiro.au/def/isotc211/DS_AssociationTypeCode)으로 표시된다.

> **NOTE**

> 속성 [`dcat:qualifiedRelation`](https://w3c.github.io/dxwg/dcat/#Property:resource_qualified_relation)과 연관 클래스 [`dcat:Relationship`](https://w3c.github.io/dxwg/dcat/#Class:Relationship)은 W3C([PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/))에 설정되고 [§ 3.3 한정된 용어](https://www.w3.org/TR/prov-o/#description-qualified-terms)에 기술된 패턴을 따른다. 그러나 [PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/)는 활동 중심적(activity-centric)이며 '~에서 파생된' 단일 경우를 제외하고는 엔티티-엔티티 관계를 지원하지 않으므로 일반적인 경우를 지원하기 위해 여기에 표시된 새로운 요소가 필요하다.

## DCAT 프로파일 
이 절은 공시 표준에 포함된 것이 아니다.

DCAT-2014 어휘([Data Catalog Vocabulary (DCAT)]( https://www.w3.org/TR/vocab-dcat-1/))는 다양한 도메인의 데이터 카탈로그에 적용할 수 있도록 확장되었다. 이러한 새로운 사양 각각은 DCAT 프로파일, 즉 DCAT에 기반을 둔 명명된 제약 집합을 구성한다 ([적합성](#conformance) 참조). 경우에 따라 프로필은 참조 DCAT 프로필에서 다루지 않는 메타데이터 필드에 대한 클래스와 속성을 추가하여 DCAT 프로필 중 하나를 확장한다.

일부 DCAT 프로필은 다음과 같다.

- DCAT-AP([DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)): *유럽의 데이터 포털을 위한 DCAT 어플리케이션 프로필*
- GeoDCAT-AP([GeoDCAT-AP: A geospatial extension for the DCAT application profile for data portals in Europe](https://semiceu.github.io/GeoDCAT-AP/releases/)): [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)의 공간 프로파일
- StatDCAT-AP([StatDCAT-AP – DCAT Application Profile for description of statistical datasets. Version 1.0.1](https://joinup.ec.europa.eu/solution/statdcat-application-profile-data-portals-europe)): [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)의 통계 프로필
- DCAT-AP_IT([DCAT-AP-IT]()): [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)의 이탈리아 프로필
- GeoDCAT-AP_IT([GeoDCAT-AP in Italy, the national guidelines published](https://joinup.ec.europa.eu/news/geodcat-apit)): [GeoDCAT-AP: A geospatial extension for the DCAT application profile for data portals in Europe](https://semiceu.github.io/GeoDCAT-AP/releases/)의 이탈리아 프로필
- DCAT-AP-NO([Standard for beskrivelse av datasett, datatjenester og datakataloger (DCAT-AP-NO)](https://data.norge.no/specification/dcat-ap-no/)): [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)의 노르웨이 프로필 
- DCAT-AP.de([Vokabulare und Dokumente für DCAT-AP.de](https://dcat-ap.de/def/)): [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)의 독일 프로필
- DCAT-BE([Linking data portals across Belgium](http://dcat.be/)): [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)의 벨기에 프로필
- DCAT-AP-SE([DCAT-AP-SE: Clarifications, translations and explanations of DCAT-AP for Sweden](https://docs.dataportal.se/dcat/en/)): [DCAT Application Profile for data portals in Europe. Version 2.0.1](https://joinup.ec.europa.eu/solution/dcat-application-profile-data-portals-europe)의 스웨덴 프로필

## 보안과 프라이버시(Security and Privacy)
DCAT 어휘는 [자격을 갖춘 관계](https://w3c.github.io/dxwg/dcat/#qualified-forms)를 통해 리소스 [생성자](https://w3c.github.io/dxwg/dcat/#Property:resource_creator), [게시자](https://w3c.github.io/dxwg/dcat/#Property:resource_publisher) 및 기타 당사자 또는 에이전트와 같은 다양한 참가자에게 데이터와 메타데이터의 귀속을 지원하고 개인 정보와 관련될 수 있는 용어를 정의한다. 또한 카탈로그된 리소스 및 배포에 관련된 [권한](https://w3c.github.io/dxwg/dcat/#Property:resource_rights)과 [라이선스](https://w3c.github.io/dxwg/dcat/#Property:resource_license)의 연결을 지원한다. 이러한 권한과 라이선스는 [ODRL Vocabulary & Expression 2.2](https://www.w3.org/TR/odrl-vocab/)에 기술된 사용자와 자산 식별자같은 민감한 정보를 잠재적으로 포함하거나 참조할 수 있다. 이러한 어휘 용어를 생성, 유지, 게시 또는 소비하는 구현은 보안과 개인 정보 보호에 대한 고려 사항을 어플리케이션 수준에서 처리되도록 보장하는 조치를 취해야 한다.

## 접근성 고려사항(Accessibility Consideration)
DCAT 어휘는 데이터 카탈로그를 설명하기 위한 모델을 제공한다. 카탈로그 데이터의 특성은 어플리케이션의 특정 도메인에 따라 다르며 텍스트가 아닌 데이터를 포함할 수 있다. 가능하다면 데이터에 대한 접근성을 향상시키기 위해 이러한 데이터의 생성과 편집을 지원하는 DCAT 프로파일 메커니즘 또는 시스템을 통해 텍스트가 아닌 데이터 리소스에 대한 대체 텍스트를 적용하는 것이 중요하다. 큰 활자, 점자, 음성, 기호 또는 더 간단한 언어와 같이 사람들이 필요로 하는 다른 형태로 변경할 수 있는 텍스트가 아닌 콘텐츠에 대한 대체 텍스트를 제공하는 관행은 [Understanding WCAG 2.0](https://www.w3.org/TR/UNDERSTANDING-WCAG20/)에 포함된 접근성 지침을 준수한다.

## Appendix A: Schema.org와의 정렬(Alignment with Schema.org)
*이 절은 공시 표준에 포함된 것이 아니다.*

> **NOTE**

> 더 많은 논의는 [Alignments and Crosswalks](https://github.com/w3c/dxwg/labels/alignment)에 대한 Issue를 참조하시오.

Schema.org([Schema.org](https://schema.org/))에는 원본 DCAT 작업 기반인 여러 타입과 속성이 포함되어 있으며(`sdo:Dataset`을 시작으로), Google의 [Dataset Search service](https://g.co/datasetsearch)에 대한 색인은 [schema.org와 DCAT](https://developers.google.com/search/docs/data-types/dataset) 모두를 사용하여 데이터세트애 대한 웹 페이지의 구조화된 기술에 의존한다. 위의 [Fig. 1](#fig-1)에 표시된 DCAT 백본을 [Fig. 7](#fig-7)의 [Schema.org](https://schema.org/)의 관련 클래스와 비교하면 특히 다음과 같은 유사성이 보인다.

- (추상) Dataset과 (구체) DataDownload의 차이은 `dcat:Dataset`과 `dcat:Distribution` 차이와 일치한다.
- Datasets와 DataCatalogs의 관계

![Fig. 7](images/schema.org-dataset.svg)
<center>Fig. 7 schema.org의 데이터세트 카탈로그 지원, 클래스와 관련된 schema.org 속성 선택</center> <a id="fig-7"></a>

메타데이터를 전혀 사용하지 않는 범용 웹 검색 서비스는 주로 [Schema.org](https://schema.org/)에 의존하므로 DCAT과 Schema.org](https://schema.org/)의 관계는 데이터세트와 서비스가 인덱스를 통해 노출되기를 원하는 데이터 제공자와 카탈로그 게시자에게 중요하다. 

[DCAT 1과 schema.org 간의 매핑](https://www.w3.org/wiki/WebSchemas/Datasets)은 데이터세트와 데이터 카탈로그를 설명하기 위해 [Schema.org](https://schema.org/)를 확장하려는 원래 제안에서 논의되었다. 이전 작업을 기반으로 구축된 [Spatial Data on the Web Working Group](https://www.w3.org/2015/spatial/wiki/ISO_19115_-_DCAT_-_Schema.org_mapping)은 DCAT 1([Data Catalog Vocabulary (DCAT)]( https://www.w3.org/TR/vocab-dcat-1/))과 [Schema.org](https://schema.org/)간의 부분 매핑을 이전에 이미 제공하였다.

수정된 DCAT(이 문서)에서 [Schema.org](https://schema.org/) 버전 3.4로의 권장 매핑은 [RDF 파일로 사용](https://w3c.github.io/dxwg/dcat/rdf/dcat-schema.ttl)할 수 있다. 이 매핑을 술어 `rdfs:subClassOf`, `rdfs:subPropertyOf`, `owl:equivalentClass`, `owl:equivalentProperty`, `skos:closeMatch`를 사용하고, 또한 주석 속성 `sdo:domainIncludes`와 `sdo:rangeIncludes`를 사용하여 [Schema.org](https://schema.org/) 의미 체계와 일치하도록 공리화하였다. 정렬은 접두사 `sdo`를 `http://schema.org/`로 간주하여 아래 표에 요약하였다.

| DCAT element | Target element from schema.org | 
|-------:|:--------------|
| **dcat:Resource** | **sdo:Thing** |
| dcterms:title	| sdo:name |
| dcterms:description	| do:description |
| dcat:keyword *dcat:keyword is singular*, *sdo:keywords is plural*	| sdo:keywords |
| dcat:theme | sdo:about |
| dcterms:identifier | sdo:identifier |
| dcterms:type | sdo:additionalType |
| dcterms:issued | sdo:datePublished |
| dcterms:modified | sdo:dateModified |
| dcterms:language | sdo:inLanguage |
| dcterms:relation | sdo:isRelatedTo |
| dcat:landingPage | sdo:url |
| dcterms:publisher | sdo:publisher |
| dcat:contactPoint| sdo:contactPoint |
| dcat:version | sdo:version |
| **dcat:Catalog** | **sdo:DataCatalog** |
| dcterms:hasPart | sdo:hasPart |
| dcat:dataset | sdo:dataset |
| dcat:distribution | sdo:distribution |
| **dcat:Dataset** | **sdo:Dataset** |
| **dcat:Dataset** *dcterms:accrualPeriodicity fixed to* `<http://purl.org/cld/freq/continuous>` | **sdo:DataFeed** |
| dcterms:spatial | sdo:spatialCoverage |
| dcterms:temporal | sdo:temporalCoverage |
| dcterms:accrualPeriodicity | sdo:repeatFrequency |
| prov:wasGeneratedBy | [ owl:inverseOf sdo:result ] |
| dcat:inSeries | sdo:isPartOf |
| **dcat:DatasetSeries** | **sdo:CreativeWorkSeries** |
| **dcat:Distribution** | **sdo:DataDownload** |
| dcterms:format | sdo:encodingFormat |
| dcat:mediaType | sdo:encodingFormat |
| dcat:byteSize | sdo:contentSize |
| dcat:accessURL | sdo:contentUrl |
| dcat:downloadURL | sdo:contentUrl |
| dcterms:license | sdo:license |
| **dcat:DataService** | **sdo:WebAPI** |
| dcat:endpointURL | sdo:url |
| dcat:endpointDescription | sdo:documentation, sdo:hasOfferCatalog |
| dcterms:type *in context of a dcat:DataService* | sdo:serviceType |
| dcat:servesDataset | sdo:serviceOutput |
| **dcat:Relationship**	| **sdo:Role** |

## Appendix B: 예(Examples)
*이 절은 공시 표준에 포함된 것이 아니다.*

### 느슨하게 구성된 카탈로그(Loosely structured catalog)

> **NOTE**

> Issue [#253](https://github.com/w3c/dxwg/issues/253)("느슨하게 구성된 카탈로그에 대한 모범 사례")에서 이 예의 배경을 논의한다.

많은 레거시 카탈로그와 리포지토리(예: CKAN)에서 '데이터세트'는 '단지 파일들의 bag'이다. 데이터세트부터 각 파일까지 배포(표현)와 다른 종류의 관계(예: 문서, 스키마, 지원 문서) 사이에 구분은 없다.

카탈로그, 저장소 또는 다른 곳에서 데이터세트와 구성 요소 리소스 간의 관계 특성을 알 수 없다면, `dcterms:relation` 또는 해당 하위 속성 `dcterms:hasPart`를 사용할 수 있다.

> EXAMPLE 55

```
ex:d33937
  dcterms:description "A set of RDF graphs representing the International [Chrono]stratigraphic Chart, ..."@en ;
  dcterms:identifier "https://doi.org/10.25919/5b4d2b83cbf2d"^^xsd:anyURI ;
  dcterms:creator <https://orcid.org/0000-0002-3884-3420> ;
  dcterms:relation ex:ChronostratChart2017-02.pdf  ;
  dcterms:relation ex:ChronostratChart2017-02.jpg ;
  dcterms:hasPart ex:timescale.zip ;
  dcterms:hasPart ex:d33937-jsonld ;
  dcterms:hasPart ex:d33937-nt ;
  dcterms:hasPart ex:d33937-rdf ;
  dcterms:hasPart ex:d33937-ttl ;
.
```

관계의 특성을 알고 있다면 [`dcterms:relation`의 다른 하위 속성을 사용](https://w3c.github.io/dxwg/dcat/#Property:resource_relation)하여 이를 전달해야 한다. 특히, 이러한 관련 리소스가 데이터세트의 적절한 표현인 것이 분명한 경우, `dcat:distribution`을 사용해야 한다.

> EXAMPLE 56

```
ex:d33937
  rdf:type dcat:Dataset ;
  dcterms:description "A set of RDF graphs representing the International [Chrono]stratigraphic Chart, ..."@en ;
  dcterms:identifier "https://doi.org/10.25919/5b4d2b83cbf2d"^^xsd:anyURI ;
  dcterms:hasFormat ex:ChronostratChart2017-02.pdf  ;
  dcterms:hasFormat ex:ChronostratChart2017-02.jpg ;
  dcat:distribution ex:timescale.zip ;
  dcat:distribution ex:d33937-jsonld ;
  dcat:distribution ex:d33937-nt ;
  dcat:distribution ex:d33937-rdf ;
  dcat:distribution ex:d33937-ttl ;
.
ex:d33937-jsonld  rdf:type dcat:Distribution ;
  dcat:downloadURL ex:isc2017.jsonld ;
  dcat:byteSize "698039"^^xsd:nonNegativeInteger ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/application/ld+json> ;
.
ex:d33937-nt  rdf:type dcat:Distribution ;
  dcat:downloadURL ex:isc2017.nt ;
  dcat:byteSize "2047874"^^xsd:nonNegativeInteger ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/application/n-triples> ;
.
ex:d33937-rdf  rdf:type dcat:Distribution ;
  dcat:downloadURL ex:isc2017.rdf ;
  dcat:byteSize "1600569"^^xsd:nonNegativeInteger ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/application/rdf+xml> ;
.
ex:d33937-ttl  rdf:type dcat:Distribution ;
  dcat:downloadURL ex:isc2017.ttl ;
  dcat:byteSize "531703"^^xsd:nonNegativeInteger ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/turtle> ;
.
```

이 예는 [DXWG 코드 리포지토리](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)의 [csiro-dap-examples.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/csiro-dap-examples.ttl)과 [csiro-stratchart_dcat3.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/csiro-stratchart_dcat3.ttl)에서 가용하다.

DCAT의 데이터세트 기술자와 함께 다른 RDF 어휘의 적절한 요소를 사용하여 관련 리소스의 특성에 대한 추가 세부 정보를 제공할 수 있다. 예를 들어 위의 예는 다음과 같이 더 완벽하게 표현될 수 있다 (포함된 주석은 그래프의 다양한 리소스를 설명한다).

> EXAMPLE 57

```
dap:d33937
  rdf:type dcat:Dataset ;
  dcterms:title "The data"@en ;
  dcterms:conformsTo <http://resource.geosciml.org/ontology/timescale/gts> ;
  dcterms:description "A set of RDF graphs representing the International [Chrono]stratigraphic Chart [...]"@en ;
  dcterms:identifier "https://doi.org/10.25919/5b4d2b83cbf2d" ;
  dcterms:issued "2018-07-07"^^xsd:date ;
  dcterms:license <https://creativecommons.org/licenses/by/4.0/> ;
  dcterms:publisher <http://www.csiro.au> ;
  dcterms:hasPart <http://stratigraphy.org/ICSchart/ChronostratChart2017-02.jpg> ;
  dcterms:hasPart <http://stratigraphy.org/ICSchart/ChronostratChart2017-02.pdf> ;
  dcterms:hasPart [
    rdf:type dcat:Dataset ;
    dcterms:conformsTo <https://www.w3.org/TR/owl2-overview/> ;
    dcterms:title "The ontology used for the data"@en ;
    dcterms:description "This is an RDF/OWL representation of the GeoSciML Geologic Timescale model ..."@en ;
    dcterms:issued "2011-01-01"^^xsd:date ;
    dcterms:modified "2017-04-28"^^xsd:date ;
    dcterms:title "Geologic Timescale model" ;
    dcterms:type <http://purl.org/adms/assettype/DomainModel> ;
    dcat:distribution [
      rdf:type dcat:Distribution ;
      dcterms:title "RDF/XML representation of the ontology used for the data"@en ;
      dcat:downloadURL <http://resource.geosciml.org/ontology/timescale/gts.rdf> ;
      dcat:mediaType <http://www.iana.org/assignments/media-types/application/rdf+xml> ;
    ] ;
    dcat:distribution [
      rdf:type dcat:Distribution ;
      dcterms:title "TTL representation of the ontology used for the data"@en ;
      dcat:downloadURL <http://resource.geosciml.org/ontology/timescale/gts.ttl> ;
      dcat:mediaType <http://www.iana.org/assignments/media-types/text/turtle> ;
    ] ;
    dcat:distribution [
      rdf:type dcat:Distribution ;
      dcterms:title "Webpage describing the ontology used for the data"@en ;
      dcat:downloadURL <http://resource.geosciml.org/ontology/timescale/gts.html> ;
      dcat:mediaType <http://www.iana.org/assignments/media-types/text/html> ;
    ] ;
    dcat:landingPage <http://resource.geosciml.org/ontology/timescale/gts> ;
  ] ;
  dcat:distribution [
    rdf:type dcat:Distribution ;
    dcterms:conformsTo <https://www.w3.org/TR/rdf-schema/> ;
    dcterms:title "RDF representation of the data"@en ;
    dcat:accessService [
      rdf:type dcat:DataService ;
      dcterms:conformsTo <https://www.w3.org/TR/sparql11-protocol/> ;
      dcterms:title "International Chronostratigraphic Chart hosted at Research Vocabularies Australia"@en ;
      dcterms:description "Service that supports queries to obtain RDF representations of subsets of the data"@en ;
      dcat:endpointURL <http://vocabs.ands.org.au/repository/api/sparql/csiro_international-chronostratigraphic-chart_2017> ;
      dcat:landingPage <https://vocabs.ands.org.au/viewById/196> ;
    ] ;
  ] ;
  dcat:distribution [
    rdf:type dcat:Distribution ;
    dcterms:identifier "isc2017.jsonld" ;
    dcterms:title "JSON-LD serialization of the RDF representation of the entire dataset"@en ;
    dcat:mediaType <http://www.iana.org/assignments/media-types/application/ld+json> ;
  ] ;
  dcat:distribution [
    rdf:type dcat:Distribution ;
    dcterms:identifier "isc2017.nt" ;
    dcterms:title "N-Triples serialization of the RDF representation of the entire dataset"@en ;
    dcat:mediaType <http://www.iana.org/assignments/media-types/application/n-triples> ;
  ] ;
  dcat:distribution [
    rdf:type dcat:Distribution ;
    dcterms:identifier "isc2017.rdf" ;
    dcterms:title "RDF/XML serialization of the RDF representation of the entire dataset"@en ;
    dcat:mediaType <http://www.iana.org/assignments/media-types/application/rdf+xml> ;
  ] ;
  dcat:distribution [
    rdf:type dcat:Distribution ;
    dcterms:identifier "isc2017.ttl" ;
    dcterms:title "TTL serialization of the RDF representation of the entire dataset"@en ;
    dcat:mediaType <http://www.iana.org/assignments/media-types/text/turtle> ;
  ] ;
  dcat:landingPage <https://data.csiro.au/dap/landingpage?pid=csiro:33937> ;
.

<http://stratigraphy.org/ICSchart/ChronostratChart2017-02.jpg>
  rdf:type foaf:Document ;
  dcterms:type <http://purl.org/dc/dcmitype/Image> ;
  dcterms:format  <http://www.iana.org/assignments/media-types/img/jpeg> ;
  dcterms:description "Coloured image representation of the International Chronostratigraphic Chart"@en ;
  dcterms:issued "2017-02-01"^^xsd:date ;
  dcterms:title "International Chronostratigraphic Chart"@en ;
.
<http://stratigraphy.org/ICSchart/ChronostratChart2017-02.pdf>
  rdf:type foaf:Document ;
  dcterms:type <http://purl.org/dc/dcmitype/Image> ;
  dcterms:format <http://www.iana.org/assignments/media-types/application/pdf> ;
  dcterms:description "Coloured image representation of the International Chronostratigraphic Chart"@en ;
  dcterms:issued "2017-02-01"^^xsd:date ;
  dcterms:title "International Chronostratigraphic Chart"@en ;
.
```

이 예는 [DXWG 코드 리포지토리](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)의 [csiro-stratchart.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/csiro-stratchart.ttl)에서 가용하다.

### 데이터세트 출처(Daraset provenance)
데이터세트의 출처 또는 비즈니스 컨텍스트는 W3C 출처 온톨로지([PROV-O: The PROV Ontology](https://www.w3.org/TR/prov-o/))의 요소를 사용하여 기술할 수 있다.

예를 들어 데이터세트 기술에서 데이터세트를 생성한 프로젝트로의 간단한 링크는 다음과 같이 형식화할 수 있다 (명확성을 위해 기타 세부 정보는 생략).

> EXAMPLE 58

```
dap:atnf-P366-2003SEPT
  rdf:type dcat:Dataset ;
  dcterms:bibliographicCitation "Burgay, M; McLaughlin, M; Kramer, M; Lyne, A; Joshi, B; Pearce, G; D'Amico, N; Possenti, A; Manchester, R; Camilo, F (2017): Parkes observations for project P366 semester 2003SEPT. v1. CSIRO. Data Collection. https://doi.org/10.4225/08/598dc08d07bb7" ;
  dcterms:title "Parkes observations for project P366 semester 2003SEPT"@en ;
  dcat:landingPage <https://data.csiro.au/dap/landingpage?pid=csiro:P366-2003SEPT> ;
  prov:wasGeneratedBy dap:P366 ;
  .

dap:P366
  rdf:type prov:Activity ;
  dcterms:type <http://dbpedia.org/resource/Observation> ;
  prov:startedAtTime "2000-11-01"^^xsd:date ;
  prov:used dap:Parkes-radio-telescope ;
  prov:wasInformedBy dap:ATNF ;
  rdfs:label "P366 - Parkes multibeam high-latitude pulsar survey"@en ;
  rdfs:seeAlso <https://doi.org/10.1111/j.1365-2966.2006.10100.x> ;
  .
```

이 예는 [DXWG 코드 리포지토리](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)의 [csiro-dap-examples.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/csiro-dap-examples.ttl)에서 가용하다.

여러 속성은 인용과 제목을 포함하여 출처 정보를 캡처하지만 프로젝트의 공식 설명에 대한 기본 링크는 `prov:wasGeneratedBy`를 통해 이루어진다. 프로젝트에 대한 간략한 설명은 `prov:Activity`로 표시되지만 반드시 동일한 카탈로그의 일부가 아닐 수도 있다. 프로젝트가 진행 중이므로 활동에 종료 날짜가 빠졌다.

추가 출처 정보는 PROV의 다른 *starting point 속성*, 특히 `prov:wasAttributedTo`(데이터세트 생산과 관련된 에이전트에 연결)와 `prov:wasDerivedFrom`(이전 데이터 세트에 연결)을 사용하여 제공될 수 있다. 이 두 가지 모두 DCAT를 보완하기 위하여 이미 사용되는 Dublin Core 속성은 다음과 같다.

- `prov:wasAttributedTo`는 `dcterms:creator`, `dcterms:contributor` 또는 `dcterms:publisher`를 사용하여 명확히 특정되지 않은 모든 종류의 관련 에이전트(예: 프로젝트 후원자, 관리자, 데이터세트 소유자 등)에 대한 일반 링크를 제공한다.
- `prov:wasDerivedFrom`은 반드시 이전 데이터세트일 필요는 없는 `dcterms:source`와 비교하여 입력 또는 선행 데이터세트에 대한 보다 구체적인 관계를 지원한다.

자원 귀속(attributiob)과 상호 관계에 대한 *한정된 속성(qualified properties)*의 사용에 대한 추가 패턴은 [15. 한정된 관계](#qualified-relations)에서 다루고 있다.

### 데이터세트와 출판물 연결(Link datasets and publications)
종종 데이터세트는 출판물(학술 논문, 보고서 등)과 연관되며 이 버전의 DCAT은 데이터세트에 대한 출판물을 데이터세트에 연결하는 방법을 제공하기 위해 `dcterms:isReferencedBy` 속성을 사용한다.

다음 예는 [Dryad repository](https://datadryad.org/)에 게시된 데이터세트를 [Nature Scientific Data 저널](https://doi.org/10.1038/sdata.2018.22)에서 제공되는 출판물에 연결하는 방법을 보여준다.

> EXAMPLE 59

```
ex:globtherm
  dcterms:title "Data from: GlobTherm, a global database on thermal tolerances for aquatic and terrestrial organisms"@en ;
  dcterms:description "How climate affects species distributions is a longstanding question receiving renewed interest owing to the need to predict the impacts of global warming on biodiversity. Is climate change forcing species to live near their critical thermal limits? Are these limits likely to change through natural selection? These and other important questions can be addressed with models relating geographical distributions of species with climate data, but inferences made with these models are highly contingent on non-climatic factors such as biotic interactions. Improved understanding of climate change effects on species will require extensive analysis of thermal physiological traits, but such data are scarce and scattered. To overcome current limitations, we created the GlobTherm database. The database contains experimentally derived species’ thermal tolerance data currently comprising over 2,000 species of terrestrial, freshwater, intertidal and marine multicellular algae, plants, fungi, and animals. The GlobTherm database will be maintained and curated by iDiv with the aim of expanding it, and enable further investigations on the effects of climate on the distribution of life on Earth."@en ;
  dcterms:identifier "https://doi.org/10.5061/dryad.1cv08"^^xsd:anyURI ;
  dcterms:creator <https://orcid.org/0000-0002-7883-3577> ;
  dcterms:relation <https://doi.org/10.5061/dryad.1cv08/6> ;
  dcterms:relation <https://doi.org/10.5061/dryad.1cv08/7> ;
  dcterms:isReferencedBy <https://doi.org/10.1038/sdata.2018.22>.
```

이 예는 [DXWG 코드 리포지토리](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)의 [dryad-globtherm-sdata.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/dryad-globtherm-sdata.ttl)에서 가용하다.

### 데이터 서비스(Data services)
데이터 서비스는 DCAT를 사용하여 기술할 수 있다. 분류기 `dcterms:type`, `dcterms:conformsTo` 및 `dcat:endpointDescription`의 값들은 실제 끝점이 `dcat:endpointURL`이 제공하는 서비스에 대해 점진적으로 더 자세한 정보를 제공한다.

첫 번째 예는 유럽 환경청(EEA)에서 호스팅하는 데이터 카탈로그를 설명한다. 이를 `dcat:DataService`로 분류되며 `dcterms:type`이 공간 데이터 서비스 타입([INSPIRE Registry: Spatial data service types](http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/))의 INSPIRE 분류에서 "발견"으로 설정하였다.

이 예는 [DXWG 코드 리포지토리](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)의 [eea-csw.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/eea-csw.ttl)에서 가용하다.

> EXAMPLE 60

```
ex:EEA-CSW-Endpoint
  rdf:type dcat:DataService ;
  dcterms:type <http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoCatalogueService> ;
  dcterms:accessRights <http://publications.europa.eu/resource/authority/access-right/PUBLIC> ;
  dcterms:conformsTo <http://www.opengis.net/def/serviceType/ogc/csw> ;
  dcterms:description "The EEA public catalogue of spatial datasets references
    the spatial datasets used by the European Environment Agency as well as
    the spatial datasets produced by or for the EEA. In the latter case,
    when datasets are publicly available, a link to the location from where
    they can be downloaded is included in the dataset's metadata. The
    catalogue has been initially populated with the most important spatial
    datasets already available on the data&maps section of the EEA website
    and is currently updated with any newly published spatial dataset."@en ;
  dcterms:identifier "eea-sdi-public-catalogue" ;
  dcterms:issued "2012-01-01"^^xsd:date ;
  dcterms:license <https://creativecommons.org/licenses/by/2.5/dk/> ;
  dcterms:spatial [
    rdf:type dcterms:Location ;
    dcat:bbox "POLYGON((-180 90,180 90,180 -90,-180 -90,-180 90))"^^gsp:wktLiteral ;
  ] ;
  dcterms:title "European Environment Agency's public catalogue of spatial datasets."@en ;
  dcterms:type <http://inspire.ec.europa.eu/metadata-codelist/ResourceType/service> ;
  dcterms:type <http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/discovery> ;
  dcat:contactPoint ex:EEA ;
  dcat:endpointDescription <https://sdi.eea.europa.eu/catalogue/srv/eng/csw?service=CSW&request=GetCapabilities> ;
  dcat:endpointURL <http://sdi.eea.europa.eu/catalogue/srv/eng/csw> ;
.
```

[EXAMPLE 61](#ex-61)은 각 서비스 기술의 `dcat:servesDataset` 속성 값으로 표시된 대로 세 가지 개별 서비스가 가능한 Geoscience Australia에서 호스팅하는 데이터세트를 보인다. 이들은 `dcat:DataService`로 분류되며 공간 데이터 서비스 타입([INSPIRE Registry: Spatial data service types](http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/))의 INSPIRE 분류에서 "다운로드"와 "보기"로 설정된 `dcterms:type`도 있다.

[EXAMPLE 61](#ex-61)은 [DXWG 코드 리포지토리](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)의 [ga-courts.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/ga-courts.ttl)에서 가용하다.

> EXAMPLE 61 <a id="ex-61"></a>

```
ga-courts:jc
  rdf:type dcat:Dataset ;
  dcterms:description "The dataset contains spatial locations, in point format, of the Australian High Court, Australian Federal Courts and the Australian Magistrates Courts."@en ;
  dcterms:spatial [
    rdf:type dcterms:Location ;
      dcat:bbox """<http://www.opengis.net/def/crs/EPSG/0/4283> POLYGON((
      -42.885989 115.864566 , -12.460578 115.864566 ,
      -12.460578 153.276835 , -42.885989 153.276835 ,
      -42.885989 115.864566 
    ))"""^^geosparql:wktLiteral ;
  ] ;
  dcterms:title "Judicial Courts"@en ;
  dcterms:type <http://purl.org/dc/dcmitype/Dataset> ;
  dcat:landingPage <https://ecat.ga.gov.au/geonetwork/srv/eng/catalog.search#/metadata/cc365600-294a-597d-e044-00144fdd4fa6> ;
.

ga-courts:jc-esri
  rdf:type dcat:DataService ;
  dcterms:conformsTo <https://developers.arcgis.com/rest/> ;
  dcterms:description "This web service provides access to the National Judicial Courts dataset and presents the spatial locations of all the known Australian High Courts, Australian Federal Courts and the Australian Federal Circuit Courts located within Australia, all complemented with feature attribution."@en ;
  dcterms:identifier "2b8540c8-4a43-144d-e053-12a3070a3ff7" ;
  dcterms:title "National Judicial Courts MapServer"@en ;
  dcterms:type <http://purl.org/dc/dcmitype/Service> ;
  dcterms:type <https://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/download> ;
  dcterms:type <https://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/view> ;
  dcat:endpointURL <http://services.ga.gov.au/gis/rest/services/Judicial_Courts/MapServer> ;
  dcat:landingPage <https://ecat.ga.gov.au/geonetwork/srv/eng/catalog.search#/metadata/2b8540c8-4a43-144d-e053-12a3070a3ff7> ;
  dcat:servesDataset ga-courts:jc ;
.

ga-courts:jc-wfs
  rdf:type dcat:DataService ;
  dcterms:conformsTo <http://www.opengis.net/def/serviceType/ogc/wfs/2.0.0> ;
  dcterms:conformsTo <http://www.opengis.net/def/serviceType/ogc/wfs/1.1.0> ;
  dcterms:conformsTo <http://www.opengis.net/def/serviceType/ogc/wfs/1.0.0> ;
  dcterms:description "This web service provides access to the National Judicial Courts dataset and presents the spatial locations of all the known Australian High Courts, Australian Federal Courts and the Australian Federal Circuit Courts located within Australia, all complemented with feature attribution."@en ;
  dcterms:identifier "2b8540c8-4a42-144d-e053-12a3070a3ff7" ;
  dcterms:title "National Judicial Courts WFS"@en ;
  dcterms:type <http://purl.org/dc/dcmitype/Service> ;
  dcterms:type <https://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/download> ;
  dcat:endpointDescription <http://services.ga.gov.au/gis/services/Judicial_Courts/MapServer/WFSServer?request=GetCapabilities&service=WFS> ;
  dcat:endpointURL <http://services.ga.gov.au/gis/services/Judicial_Courts/MapServer/WFSServer> ;
  dcat:landingPage <https://ecat.ga.gov.au/geonetwork/srv/eng/catalog.search#/metadata/2b8540c8-4a42-144d-e053-12a3070a3ff7> ;
  dcat:servesDataset ga-courts:jc ;
.

ga-courts:jc-wms
  rdf:type dcat:DataService ;
  dcterms:conformsTo <http://www.opengis.net/def/serviceType/ogc/wms/1.3> ;
  dcterms:description "This web service provides access to the National Judicial Courts dataset and presents the spatial locations of all the known Australian High Courts, Australian Federal Courts and the Australian Federal Circuit Courts located within Australia, all complemented with feature attribution."@en ;
  dcterms:identifier "2b8540c8-4a41-144d-e053-12a3070a3ff7" ;
  dcterms:title "National Judicial Courts WMS"@en ;
  dcterms:type <http://purl.org/dc/dcmitype/Service> ;
  dcterms:type <https://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/view> ;
  dcat:endpointDescription <http://services.ga.gov.au/gis/services/Judicial_Courts/MapServer/WMSServer?request=GetCapabilities&service=WMS> ;
  dcat:endpointURL <http://services.ga.gov.au/gis/services/Judicial_Courts/MapServer/WMSServer> ;
  dcat:landingPage <https://ecat.ga.gov.au/geonetwork/srv/eng/catalog.search#/metadata/2b8540c8-4a41-144d-e053-12a3070a3ff7> ;
  dcat:servesDataset ga-courts:jc ;
.
```

### 압축과 패키지 배포(Compressed and packaged distributions)
첫 번째 예는 배포용으로 GZIP 파일로 압축된 다운로드 가능한 파일을 제공한다.

> EXAMPLE 62

```
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix dcterms: <http://purl.org/dc/terms/> .

<https://data.gov.cz/zdroj/datová-sada/247025684/22> a dcat:Distribution ;
  dcat:downloadURL <https://mvcr1.opendata.cz/czechpoint/2007.csv.gz> ;
  dcterms:license <https://data.gov.cz/podmínky-užití/volný-přístup/> ;
  dcterms:conformsTo <https://mvcr1.opendata.cz/czechpoint/2007.json> ;
  dcterms:format <http://publications.europa.eu/resource/authority/file-type/CSV> ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/csv> ;
  dcat:compressFormat <http://www.iana.org/assignments/media-types/application/gzip>
.
```

첫 번째 예는 배포용으로 여러 파일을 TAR 파일로 패키지한 다운로드 가능한 파일을 제공한다.

> EXAMPLE 63

```
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix dcterms: <http://purl.org/dc/terms/> .

<https://data.gov.cz/zdroj/datová-sada/247025684/22> a dcat:Distribution ;
  dcat:downloadURL <https://mvcr1.opendata.cz/czechpoint/data.tar> ;
  dcterms:license <https://data.gov.cz/podmínky-užití/volný-přístup/> ;
  dcterms:conformsTo <https://mvcr1.opendata.cz/czechpoint/2007.json> ;
  dcterms:format <http://publications.europa.eu/resource/authority/file-type/CSV> ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/csv> ;
  dcat:packageFormat <http://publications.europa.eu/resource/authority/file-type/TAR>
.
```

세 번째 예는 배포용으로 여러 파일을 TAR 파일로 패키지한 후 GZIP 파일로 압축된 파일을 제공한다.

> EXAMPLE 64

```
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix dcterms: <http://purl.org/dc/terms/> .

<https://data.gov.cz/zdroj/datová-sada/247025684/22> a dcat:Distribution ;
  dcat:downloadURL <https://mvcr1.opendata.cz/czechpoint/data.tar.gz> ;
  dcterms:conformsTo <https://mvcr1.opendata.cz/czechpoint/2007.json> ;
  dcterms:license <https://data.gov.cz/podmínky-užití/volný-přístup/> ;
  dcterms:format <http://publications.europa.eu/resource/authority/file-type/CSV> ;
  dcat:mediaType <http://www.iana.org/assignments/media-types/text/csv> ;
  dcat:packageFormat <http://publications.europa.eu/resource/authority/file-type/TAR> ;
  dcat:compressFormat <http://www.iana.org/assignments/media-types/application/gzip>
  .
```
위의 예들은 [DXWG 코드 리포지토리](https://github.com/w3c/dxwg/tree/gh-pages/dcat/examples)의 [compress-and-package.ttl](https://w3c.github.io/dxwg/dcat/examples/vocab-dcat-3/compress-and-package.ttl)에서 가용하다.
