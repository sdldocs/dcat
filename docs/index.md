# Data Catalog Vocabulary (DCAT) - Version 3
[W3C Data Catalog Vocabulary (DCAT) - Version 3](https://www.w3.org/TR/vocab-dcat-3/)<sup>[1](#footnote_1)</sup>를 편역한 것임. 또한 윤 준석님이 이미 일부분 번역[Data Catalog Vocabulary (DCAT)](https://phobyjun.github.io/2019/07/15/Data-Catalog-Vocabulary-(DCAT).html)한 것을 복사하여 수정하였고, 최근 발표된 version 3에 따라 첨가하였고, 또한 필요에 따라 번역하지 않았던 부분을 추가하였다.

## 요약
DCAT는 웹에 게시된 데이터 카탈로그들간 상호운용(interoperability)을 하기 위해 고안된 RDF 용어(vocabulary)이다. 이 문서에서는 스키마를 정의하고 스키마의 사용 예를 보인다.

DCAT를 활용하여 게시자(publisher)는 여러 카탈로그로부터 메타데이터를 수집하고 종합할 수 있도록 표준 모델과 용어를 사용하여 카탈로그의 데이터세트와 데이터 서비스를 기술할 수 있다. 이를 통해 데이터세트와 데이터 서비스의 검색 효율을 높일 수 있다. 또한 데이터 카탈로그를 분산된 접근 방식으로 게시할 수 있으며, 동일한 쿼리 메커니즘과 구조를 사용하여 여러 사이트의 카탈로그를 접근하여 데이터세트를 검색할 수 있다. 통합된 DCAT 메타데이터는 디지털 보존 프로세스의 일부로 매니페스트 파일 역할을 할 수 있다.

- DCAT 용어의 namespace는 [`http://www.w3.org/ns/dcat#`](http://www.w3.org/ns/dcat#)
- DCAT namespace의 접두어(prefix)로 `dcat`의 사용을 권고한다. 

<a name="footnote_1">1</a>: fourth public working draft of 10 May 2022 기준
